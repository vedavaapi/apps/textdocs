import json
from pprint import pprint

from lxml import etree
from vedavaapi.client import VedavaapiSession, objstore
from ...markup import parser


def get_ec_graph(vc: VedavaapiSession, root_text_doc_id):
    resp = objstore.get_graph(
        vc, {"_id": root_text_doc_id},
        [{"source": {"jsonClass": {"$in": ["TextDocument", "TextSection", "Sequence"]}}}],
        direction='referred', max_hops=-1, json_class_projection_map={"*": {"resolvedPermissions": 0}},
        hop_inclusions_config=["lambda r: r.get('jsonClass') == 'TextDocument'"]
    )
    graph = resp['graph']
    ec_graph = None
    if len(graph):
        text_doc = list(graph.values())[0]
        ec_graph_markup = text_doc.get('ec_graph_markup')
        if ec_graph_markup:
            ec_graph = parser.parse_directives_to_ec_graph(ec_graph_markup)

    return ec_graph


class TextDocumentElement(object):

    def __init__(self, doc, ec: parser.ElementClass, hierarchy: list, relation, id_adapter=None):
        self.doc = doc
        self.meta_map = dict((m['label'], m['value']) for m in self.doc.get('metadata', []))
        self.ec = ec
        self.hierarchy = [self] + hierarchy
        self.relation = relation
        self.id_adapter = id_adapter

        self.attr_map = {}
        self.children = []

    def text(self):
        if 'content' in self.doc:
            return self.doc['content']
        return ''.join(child.text() for child in self.children)

    def name(self):
        return self.doc.get('name')

    def _id(self):
        doc_id = self.doc.get('_id')
        if not callable(self.id_adapter):
            return doc_id
        return self.id_adapter(doc_id)

    @classmethod
    def from_text_doc(
            cls, text_doc, graph, ec_graph: parser.ElementClassGraph,
            hierarchy=None, relation=None, id_adapter=None):
        elem = cls(
            text_doc, ec_graph.create(text_doc.get('jsonClassLabel', '_')),
            hierarchy or [], relation, id_adapter=id_adapter
        )
        reached_ids = text_doc.get('_reached_ids', {}).get('source', [])
        children = []
        if reached_ids:
            seq_id = reached_ids[0]
            seq = graph.get(seq_id)
            if seq:
                children = [graph.get(cid) for cid in seq.get('_order', []) if cid in graph]
        child_elems = [
            cls.from_text_doc(
                child, graph, ec_graph, hierarchy=hierarchy,
                relation=child.get('relation_to_parent'), id_adapter=id_adapter
            )
            for child in children
        ]
        elem.children.extend(child_elems)
        return elem

    @classmethod
    def get_html_class(cls, ec_name):
        return 'vvm-ec-{}'.format(ec_name)

    def html(self):
        html_elem_tag = self.ec.metadata.get('htag')
        explicit_html_tag = html_elem_tag is not None
        html_elem_tag = html_elem_tag or 'div'

        #  html_elem_style = self.ec.metadata.get('style')
        #  html_elem_tag = 'div' if self.doc.get('components') else 'div'  # NOTE
        html_elem_class = 'vvm-ec {}'.format(self.get_html_class(self.ec.name))
        if self.relation == parser.ElementClassGraph.ATTR_OF:
            html_elem_class += ' vvm-rel-attr-of'

        html_elem = etree.Element(html_elem_tag, attrib={
            "class": html_elem_class,
            "id": self._id(),
            "data-ec": self.ec.name,
            "data-ec-label": self.ec.label(),
            "title": ' / '.join([
                '{}{}'.format(m.ec.label(), (' - {}'.format(m.name()) if m.name() else ''))
                for m in reversed(self.hierarchy)
            ])
        })

        # if html_elem_style:
        #    html_elem.attrib['style'] = html_elem_style

        html_elem.attrib.update(dict(
            ('data-{}'.format(m['label']), m['value'])
            for m in self.doc.get('metadata', [])
        ))

        def append_text(elem, text):
            text_parts = str(text).split('\n')
            elem.text = text_parts[0]
            for tp in text_parts[1:]:
                br = etree.SubElement(elem, 'br')
                br.tail = tp

        components = self.doc.get('components')
        if not components and isinstance(self.doc.get('content'), str):
            components = [{"chars": self.doc['content']}]
            
        if components:
            single_component = len(components) == 1
            new_line_in_component = False  # matter only for len(components) == 1
            for component in components:
                anno_elem = etree.SubElement(html_elem, 'span', attrib={
                    "class": 'vvm-ec-_anno',
                })
                if component.get('origin'):
                    anno_elem.attrib['data-origin'] = component['origin']
                if single_component:
                    new_line_in_component = '\n' in component['chars']
                append_text(anno_elem, component['chars'])
            # NOTE BELOW! CUSTOM HANDLING
            if single_component and not new_line_in_component and not explicit_html_tag:
                html_elem.tag = 'span'
            return html_elem

        name_child_wrapper = etree.SubElement(
            html_elem, 'p', attrib={"class": "vvm-elem-name-wrapper"}
        )
        name_child = etree.SubElement(
            name_child_wrapper, 'span', attrib={"class": "vvm-elem-name"}
        )
        if self.name() is not None:
            append_text(name_child, '{} - {}'.format(self.ec.label(), self.name()))

        children_wrapper = etree.SubElement(html_elem, 'div', attrib={"class": "vvm-elem-children"})

        child_parallels = []
        current_child_parallel = None
        prev_elem = None

        for child_elem in self.children:
            if prev_elem and (prev_elem.ec == child_elem.ec):
                branch = current_child_parallel[prev_elem.ec.name]
                branch.append(child_elem)
                prev_elem = child_elem
                continue
            if prev_elem and (child_elem.ec in prev_elem.ec.parallels):
                current_child_parallel[child_elem.ec.name] = current_child_parallel.get(child_elem.ec.name, [])
                branch = current_child_parallel[child_elem.ec.name]
                branch.append(child_elem)
                prev_elem = child_elem
                continue
            current_child_parallel = {}
            child_parallels.append(current_child_parallel)
            current_child_parallel[child_elem.ec.name] = [child_elem]
            prev_elem = child_elem

        def extend_children(_root_html_elem, _child_branch):
            if not len(_child_branch):
                return False
            #  branch_ec_class = _child_branch[0].ec.name
            for i, child in enumerate(_child_branch):
                child_html_elem = child.html()
                _root_html_elem.append(child_html_elem)

        for cp_count, child_parallel in enumerate(child_parallels):
            if not len(child_parallel):
                continue
            elif len(child_parallel) == 1:
                branch = list(child_parallel.values())[0]
                extend_children(children_wrapper, branch)
            else:
                cp_elem = etree.SubElement(children_wrapper, 'div', {
                    "class": "vvm-parallels",
                    "id": 'vvm-cps-{}-{}'.format(
                        str(hash(html_elem)), cp_count
                    )
                })
                for branch_count, ec_name in enumerate(sorted(child_parallel.keys())):
                    branch = child_parallel[ec_name]
                    branch_elem = etree.SubElement(cp_elem, 'div', {
                        "class": "vvm-parallel-branch",
                        "id": "vvm-cpb-{}-{}-{}".format(
                            str(hash(html_elem)), cp_count, branch_count
                        )
                    })
                    title_elem = etree.SubElement(branch_elem, 'h3', {
                        "class": "vvm-parallel-branch-title"
                    })
                    append_text(
                        title_elem, branch[0].ec.metadata.get('label', branch[0].ec.name)
                    )
                    extend_children(branch_elem, branch)

        return html_elem


def root_text_doc_to_html(elem: TextDocumentElement, ec_graph: parser.ElementClassGraph):
    document_html_elem = etree.Element('html')
    style_string = ''
    for ec_name, ec in ec_graph.graph.items():
        if 'style' not in ec.metadata:
            continue
        style_string += '.{}'.format(TextDocumentElement.get_html_class(ec_name)) + ' {' + ec.metadata['style'] + '}'

    if style_string:
        style_elem = etree.SubElement(document_html_elem, 'style')
        style_elem.text = style_string

    body_elem = etree.SubElement(document_html_elem, 'body')
    body_elem.append(elem.html())
    document_content = etree.tounicode(document_html_elem, method='html').replace('\n', '<br>')
    return document_content


def get_elem_tree(vc, root_text_doc_id, ec_graph: parser.ElementClassGraph):
    resp = objstore.get_graph(
        vc, {"_id": root_text_doc_id},
        [{"source": {"jsonClass": {"$in": ["TextDocument", "TextSection", "Sequence"]}}}],
        direction='referrer', max_hops=-1,
        json_class_projection_map={"*": {"resolvedPermissions": 0}}
    )
    graph = resp['graph']
    if root_text_doc_id not in graph:
        return None
    root_text_doc = graph[root_text_doc_id]

    root_elem = TextDocumentElement.from_text_doc(root_text_doc, graph, ec_graph)
    return root_elem


def text_doc_hierarchy_to_html(vc, root_text_doc_id, ec_graph: parser.ElementClassGraph):
    elem_tree = get_elem_tree(vc, root_text_doc_id, ec_graph)
    return root_text_doc_to_html(elem_tree, ec_graph)


def export(vc, root_text_doc_id):
    ec_graph = get_ec_graph(vc, root_text_doc_id) or parser.ElementClassGraph()
    return text_doc_hierarchy_to_html(vc, root_text_doc_id, ec_graph)


def export_from_blank_graph(blank_graph, blank_id_to_uid_map, seq_members_map):
    def id_adapter(blank_id):
        return blank_id_to_uid_map[blank_id]

    root_node_blank_id = None

    #  print(seq_members_map, blank_graph.keys())

    for n, seq_id in enumerate(seq_members_map):
        seq = blank_graph[seq_id]
        parent = blank_graph[seq['source']]
        if parent['jsonClass'] == 'TextDocument':
            root_node_blank_id = parent['_id']
        parent['_reached_ids'] = {"source": [seq_id]}
        seq['_order'] = seq_members_map[seq_id]
        seq['_reached_ids'] = {"source": seq.get('_order', [])}

    if not root_node_blank_id:
        root_node_blank_id = list(blank_id_to_uid_map.keys())[0]

    root_node = blank_graph[root_node_blank_id]
    ec_graph_markup = root_node['ec_graph_markup']
    ec_graph = parser.parse_directives_to_ec_graph(ec_graph_markup) or parser.ElementClassGraph()
    root_elem = TextDocumentElement.from_text_doc(root_node, blank_graph, ec_graph, id_adapter=id_adapter)
    return root_text_doc_to_html(root_elem, ec_graph)


"""
from vvtextdocs.helpers.exporters import html
from vedavaapi.client import VedavaapiSession
vc = VedavaapiSession('https://services.vedavaapi.org:8443/api')
vc.signin('info@vedavaapi.org', '1234')
from importlib import reload
reload(html)
ec_graph = html.get_ec_graph(vc, '5d6fe4b6410f3a000825a907')
"""
