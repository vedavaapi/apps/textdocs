import functools
import json
import re
import sys

from celery.exceptions import Ignore
from requests import HTTPError
from vedavaapi.client import VedavaapiSession, objstore


def default_update_state(state='PROGRESS', meta=None):
    print('update_state {}: {}'.format(state, str(meta)), file=sys.stderr)


def get_page_ids(vc: VedavaapiSession, book_id, pages_range=None, update_state=None):
    if book_id is None:
        return None

    update_state = update_state or default_update_state

    print('getting book graph')
    start_nodes_selector = {"_id": book_id}
    traverse_key_filter_maps_list = [
        {
            "target": {"jsonClass": "SequenceAnnotation", "canonical": "default_canvas_sequence"}
        }
    ]
    try:
        response = objstore.get_graph(
            vc, start_nodes_selector, traverse_key_filter_maps_list,
            include_incomplete_paths=True, direction='referrer', max_hops=1,
            json_class_projection_map={
                "*": {"_id": 1, "jsonClass": 1, "selector": 1, "index": 1, "_reached_ids": 1, "body": 1}
            }
        )
        print("got book graph")
    except HTTPError as e:
        update_state(state='FAILURE', meta={
            "exc_type": "ObjModelException",
            "exc_message": ['error in retrieving book graph {}'.format(str(e))],
        })
        raise Ignore()

    response_graph = response['graph']
    book = response_graph.get(book_id)
    if not book:
        update_state(state='FAILURE', meta={
            "exc_type": "ObjModelException",
            "exc_message": ['book with _id {} is not accessible'.format(book_id)],
        })
        raise Ignore()

    seq_anno_ids = book.get('_reached_ids', {}).get('target', None)
    if isinstance(seq_anno_ids, list) and len(seq_anno_ids):
        seq_anno_id = seq_anno_ids[0]
        seq_anno = response_graph[seq_anno_id]
        seq_anno_members = seq_anno.get('body', {}).get('members', [])
        sequenced_ids = [m['resource'] for m in seq_anno_members]
    else:
        selector_doc = {"jsonClass": "ScannedPage", "source": book_id}
        projection = {"selector": 1, "index": 1, "_id": 1}
        pages_resp = vc.get(
            'objstore/v1/resources',
            parms={"selector_doc": json.dumps(selector_doc), "projection": json.dumps(projection)})
        try:
            pages_resp.raise_for_status()
        except HTTPError as e:
            update_state(state='FAILURE', meta={
                "exc_type": "HTTPError",
                "exc_message": ['error in retrieving pages of book {}: {}'.format(book_id, str(e))],
            })
            raise Ignore()
        pages = pages_resp.json()['items']
        sequenced_ids = [p['_id'] for p in pages]

    if (not isinstance(pages_range, list)
            or not len(pages_range) == 2
            or False in [isinstance(b, int) for b in pages_range]):
        return sequenced_ids

    return sequenced_ids[pages_range[0]: pages_range[1]]


def get_regions_layout_graphs(vc: VedavaapiSession, page_ids, update_state=None):
    update_state = update_state or default_update_state
    start_nodes_selector = {"jsonClass": "ScannedPage", "_id": {"$in": page_ids}}

    traverse_key_filter_maps_list = [
        {"source": {"jsonClass": "ImageRegion"}},
        {"target": {"jsonClass": "TextAnnotation"}}
    ]
    direction = 'referrer'
    max_hops = 2
    json_class_projection_map = {"*": {"resolvedPermissions": 0}}

    try:
        print('getting regions graph')
        regions_graph_response = objstore.get_graph(
            vc, start_nodes_selector, traverse_key_filter_maps_list,
            include_incomplete_paths=True, direction=direction, max_hops=max_hops,
            json_class_projection_map=json_class_projection_map
        )
        regions_graph = regions_graph_response['graph']
        print('regions graph retrieved')
    except HTTPError as e:
        update_state(state='FAILURE', meta={
            "exc_type": "HTTPError",
            "exc_message": ['error in retrieving regions graph: {}'.format(str(e))],
        })
        raise Ignore()

    l_start_nodes_selector = {"_id": {"$in": page_ids}, "jsonClass": "ScannedPage"}
    l_traverse_key_filter_maps_list = [
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRPage"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRCArea"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRParagraph"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRLine"}}
    ]

    l_direction = 'referrer'
    l_max_hops = 4
    l_json_class_projection_map = {
        "*": {"_id": 1, "jsonClass": 1, "index": 1, "members": 1, "source": 1, "_reached_ids": 1}
    }

    try:
        print('getting hierarchy graph')
        layout_graph_response = objstore.get_graph(
            vc, l_start_nodes_selector, l_traverse_key_filter_maps_list,
            include_incomplete_paths=True, direction=l_direction, max_hops=l_max_hops,
            json_class_projection_map=l_json_class_projection_map
        )
        layout_graph = layout_graph_response['graph']
    except HTTPError:
        print('no hierarchy exists')
        layout_graph = None

    return regions_graph, layout_graph


def _ordered_regions_by_co_ords(regions):
    def _sort_key_fn(_region):
        fragment_selector = _region['selector']['default']
        fragment_val = fragment_selector['value']
        fragment_val_regex = r'xywh=(?P<x>[0-9]+),(?P<y>[0-9]+),(?P<w>[0-9]+),(?P<h>[0-9]+)'
        x, y, w, h = re.match(fragment_val_regex, fragment_val).groups()
        return int(y), int(x)

    sorted_regions = sorted(regions, key=_sort_key_fn)
    return sorted_regions


def get_ordered_regions(page_id, layout_graph, regions_graph):

    page_v1 = regions_graph.get(page_id)
    if not page_v1:
        return None

    region_ids = page_v1.get('_reached_ids', {}).get('source', None)

    if region_ids is None:
        return None

    def _default_ordered_regions():
        _regions = [regions_graph[rid] for rid in region_ids if rid in regions_graph]
        return _ordered_regions_by_co_ords(_regions)

    if not layout_graph:
        return _default_ordered_regions()

    page_v2 = layout_graph.get(page_id)
    if not page_v2 or not page_v2.get('_reached_ids', {}).get('source', None):
        return _default_ordered_regions()

    regions = []

    def _extend_regions(layout_elem):
        if not layout_elem:
            return
        member_resource_ids = layout_elem.get('members', {}).get('resource_ids', None)
        if member_resource_ids:
            regions.extend(
                [regions_graph[rid] for rid in member_resource_ids
                 if rid in regions_graph]
            )
            return
        reached_layout_elem_ids = layout_elem.get('_reached_ids', {}).get('source', None)
        if not reached_layout_elem_ids:
            return
        for reached_elem_id in reached_layout_elem_ids:
            _extend_regions(layout_graph.get(reached_elem_id))

    _extend_regions(page_v2)  # NOTE : page passed
    return regions


def content_from_regions(region_anno_graph, sorted_regions, wrap_in_spans):
    content = ''
    for region in sorted_regions:
        anno_ids = region.get('_reached_ids', {}).get('target', None)
        if not anno_ids:
            continue
        anno_id = anno_ids[0]
        anno = region_anno_graph[anno_id]
        anno_chars = anno.get('body', [{}])[0].get('chars').replace('\n', '<br>')
        #  print(anno_chars)
        content += '<span class="{cls}" id="{id}">{txt}</span>'.format(
            cls='vv-text-anno',
            id=anno_id,
            txt=anno_chars
        ) if wrap_in_spans else anno_chars
    #  print(content, wrap_in_spans)
    return content


def batch_it(batch_size, result_reducer=None):
    def wrapper(func):
        @functools.wraps(func)
        def decorated(all_items, *args, **kwargs):
            if not isinstance(all_items, list):
                return func(all_items, *args, **kwargs)
            reduced_result = None
            no_batches = len(all_items) // batch_size + 1
            for batch_count in range(no_batches):
                batch_context = {"current": batch_count, "total": no_batches, "size": batch_size, "total_items": len(all_items)}
                start = batch_count * batch_size
                batch = all_items[start: start + batch_size]
                if not len(batch):
                    break
                result = func(batch, *args, **kwargs, batch_context=batch_context)
                if callable(result_reducer):
                    reduced_result = result_reducer(reduced_result, result) if batch_count > 0 else result
            return reduced_result
        return decorated
    return wrapper


# noinspection PyUnusedLocal
@batch_it(15, result_reducer=lambda r, n: r + n)
def _get_html_content(
        page_ids, vc: VedavaapiSession, wrap_in_spans, update_state=None, batch_context=None, **kwargs):

    if not isinstance(page_ids, list):
        return None
    update_state = update_state or default_update_state

    content = ''
    regions_graph, layout_graph = get_regions_layout_graphs(
        vc, page_ids, update_state=update_state
    )
    for page_id in page_ids:
        ordered_regions = get_ordered_regions(page_id, layout_graph, regions_graph)
        if ordered_regions is None:
            continue
        page_content = content_from_regions(regions_graph, ordered_regions, wrap_in_spans)
        content += page_content

    if batch_context and None not in [
        batch_context.get(k) for k in ('current', 'size', 'total')
    ]:
        update_state(state='PROGRESS', meta={
            "status": "retrieving pages and their annotations",
            "current": (batch_context['current'] + 1) * batch_context['size'],
            "total": batch_context.get('total_items') or (
                    batch_context['total'] * batch_context['size']
            )
        })
    return content


def export_as_html(vc: VedavaapiSession, book_id=None, pages_range=None, page_ids=None, wrap_in_spans=True, update_state=None):
    update_state = update_state or default_update_state
    if page_ids is not None:
        resolved_page_ids = page_ids
    else:
        update_state(state='PROGRESS', meta={
            "status": "getting page ids"
        })
        resolved_page_ids = get_page_ids(
            vc, book_id, pages_range=pages_range, update_state=update_state
        )

    #  print(book_id, pages_range, page_ids, len(resolved_page_ids or []))

    if resolved_page_ids is None:
        update_state(state='FAILURE', meta={
            "exc_type": "PolicyError",
            "exc_message": ['invalid page ids'],
        })
        raise Ignore()

    update_state(
        state='PROGRESS', meta={"status": "initializing getting pages content"}
    )
    content = _get_html_content(resolved_page_ids, vc, wrap_in_spans, update_state=update_state)
    return content
