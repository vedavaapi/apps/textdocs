from lxml import etree
from vedavaapi.client import VedavaapiSession, objstore


def textdoc_to_html(vc: VedavaapiSession, textdoc_id):

    textdoc_graph_resp = objstore.get_graph(
        vc, {"_id": textdoc_id},
        [{"source": {"jsonClass": {"$in": ["TextDocument", "TextSection", "Sequence"]}}}],
        direction='referrer', max_hops=-1, json_class_projection_map={"*": {"resolvedPermissions": 0, "components": 0}})
    textdoc_graph = textdoc_graph_resp['graph']

    def set_attribs(textdoc, elem: etree.Element):
        elem.set('id', textdoc.get('_id'))
        elem.set('class', 'vv-object-{}'.format(textdoc.get('jsonClassLabel', '')))

    def _add_textdoc_branch(root_textdoc_id, elem: etree.Element):
        root_textdoc = textdoc_graph[root_textdoc_id]
        root_elem_tag = 'span' if ('content' in root_textdoc) else 'div'
        root_elem = etree.SubElement(elem, root_elem_tag)
        set_attribs(root_textdoc, root_elem)

        if 'content' in root_textdoc:
            root_elem.text = root_textdoc['content']
            return

        if '_reached_ids' not in root_textdoc:
            return

        reached_ids = root_textdoc['_reached_ids']
        reached_ids_through_source = reached_ids.get('source')
        if not reached_ids_through_source:
            return

        sequence = textdoc_graph[reached_ids_through_source[0]]
        sequenced_child_ids = sequence.get('_order')
        if not sequenced_child_ids:
            return

        for child_id in sequenced_child_ids:
            if child_id not in textdoc_graph:
                continue
            _add_textdoc_branch(child_id, root_elem)

    document = etree.Element('div')
    if textdoc_id in textdoc_graph:
        _add_textdoc_branch(textdoc_id, document)

    return etree.tounicode(document, method='html')
