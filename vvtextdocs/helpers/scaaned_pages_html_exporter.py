from lxml import etree
from requests import HTTPError
from vedavaapi.client import objstore, ObjModelException
from vedavaapi.client import VedavaapiSession


def get_tree(graph, root_node_id, traversed_graph_ids, sort_fns=None):
    if root_node_id not in graph:
        return None
    sort_fns = sort_fns or [None]

    tree = {}
    root_node = graph[root_node_id]

    tree['content'] = root_node
    traversed_graph_ids.append(root_node_id)  # NOTE depth first loop resolve.
    tree['specific_resources'] = []
    tree['annotations'] = []

    reached_ids = root_node.get('_reached_ids')
    if not reached_ids:
        return tree

    links_conf = {
        "source": {"group": "specific_resources", "linked_ids": reached_ids.get("source", None)},
        "target": {"group": "annotations", "linked_ids": reached_ids.get("target", None)},
    }
    for link, conf in links_conf.items():
        linked_ids = conf['linked_ids']
        if not linked_ids:
            continue
        tree[conf['group']] = [get_tree(graph, lid, traversed_graph_ids, sort_fns=sort_fns[1:]) for lid in
                               linked_ids if lid not in traversed_graph_ids]
    if sort_fns[0]:
        tree['specific_resources'] = sorted(tree['specific_resources'], key=lambda b: sort_fns[0](b['content']))

    return tree


def get_ordered_region_anno_pairs(page_ids, regions_trees, hierarchy_trees):
    ordered_region_anno_pairs = []

    for page_id in page_ids:
        prt = regions_trees.get(page_id, None)  # page_regions_tree
        pht = hierarchy_trees.get(page_id, None)  # page hierarchy tree

        if not prt:
            continue

        if not pht:
            region_anno_pairs = [
                (rb['content']['_id'], rb['annotations'][0]['content']['_id'])
                for rb in prt['specific_resources']
            ]

        else:
            region_anno_ids_map = dict(
                (rb['content']['_id'], rb['annotations'][0]['content']['_id'])
                for rb in prt['specific_resources']
            )
            region_anno_pairs = []
            for hocr_page_branch in pht['specific_resources']:
                for hocr_carea_branch in hocr_page_branch['specific_resources']:
                    for hocr_pg_branch in hocr_carea_branch['specific_resources']:
                        for hocr_line_branch in hocr_pg_branch['specific_resources']:
                            hocr_line_elem = hocr_line_branch['content']
                            member_resource_ids = hocr_line_elem.get('members', {}).get('resource_ids', [])
                            region_anno_pairs += [
                                (rid, region_anno_ids_map[rid])
                                for rid in member_resource_ids if rid in region_anno_ids_map
                            ]
        ordered_region_anno_pairs.extend(region_anno_pairs)

    return ordered_region_anno_pairs


def content_from_sorted_regions(regions_graph, ordered_region_anno_pairs):
    document = etree.Element('div')
    for region_id, anno_id in ordered_region_anno_pairs:
        anno = regions_graph[anno_id]
        anno_span = etree.HTML('<span>{}</span>'.format(anno.get('body', [{}])[0].get('chars').replace('\n', '<br>')))[0][0]
        document.append(anno_span)

        #  anno_span = etree.SubElement(document, 'span')
        anno_span.set('class', 'vv-text-anno')
        anno_span.set('id', anno.get('_id'))
        #  anno_span.text = (anno.get('body', [{}])[0].get('chars'))

    return document


def export_to_html(vc: VedavaapiSession, page_ids):

    start_nodes_selector = {"jsonClass": "ScannedPage", "_id": {"$in": page_ids}}

    traverse_key_filter_maps_list = [
        {"source": {"jsonClass": "ImageRegion"}},
        {"target": {"jsonClass": "TextAnnotation"}}
    ]
    direction = 'referrer'
    max_hops = 2
    json_class_projection_map = {"*": {"resolvedPermissions": 0}}

    try:
        print('getting regions graph')
        regions_graph_response = objstore.get_graph(
            vc, start_nodes_selector, traverse_key_filter_maps_list,
            include_incomplete_paths=False, direction=direction, max_hops=max_hops,
            json_class_projection_map=json_class_projection_map
        )
        regions_graph = regions_graph_response['graph']
        print('regions graph retrieved')
    except HTTPError as e:
        raise ObjModelException(
            'error in getting regions graph',
            status_code=e.response.status_code, attachments={"error": str(e)}
        )

    print('retrieving hierarchy')

    h_start_nodes_selector = {"_id": {"$in": page_ids}, "jsonClass": "ScannedPage"}
    h_traverse_key_filter_maps_list = [
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRPage"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRCArea"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRParagraph"}},
        {"source": {"jsonClass": "Resource", "jsonClassLabel": "HOCRLine"}}
    ]

    h_direction = 'referrer'
    h_max_hops = 4
    h_json_class_projection_map = {
        "*": {"_id": 1, "jsonClass": 1, "index": 1, "members": 1, "source": 1, "_reached_ids": 1}
    }

    try:
        print('getting hierarchy graph')
        hierarchy_graph_response = objstore.get_graph(
            vc, h_start_nodes_selector, h_traverse_key_filter_maps_list,
            include_incomplete_paths=False, direction=h_direction, max_hops=h_max_hops,
            json_class_projection_map=h_json_class_projection_map
        )
        hierarchy_graph = hierarchy_graph_response['graph']
    except HTTPError:
        print('no hierarchy exists')
        hierarchy_graph = None

    print('creating trees')
    regions_trees = {}
    hierarchy_trees = {}
    spr_sorter_fn = lambda r: r.get('index', 9999999)

    for page_id in page_ids:
        page_regions_tree = get_tree(regions_graph, page_id, [])
        page_hierarchy_tree = get_tree(hierarchy_graph, page_id, [], sort_fns=[spr_sorter_fn] * 4)
        #  print(page_regions_tree, page_hierarchy_tree)
        regions_trees[page_id] = page_regions_tree
        hierarchy_trees[page_id] = page_hierarchy_tree

    ordered_region_anno_pairs = get_ordered_region_anno_pairs(page_ids, regions_trees, hierarchy_trees)
    #  print(ordered_region_anno_pairs)

    html_doc = content_from_sorted_regions(regions_graph, ordered_region_anno_pairs)

    return html_doc


def get_pages_sort_fn(sequence_anno):
    sequence_anno_members = sequence_anno.get('body', {}).get('members', None) if sequence_anno else None
    sequenced_ids = [m['resource'] for m in sequence_anno_members] if sequence_anno_members else None
    #  print(sequenced_ids)

    def _sort_key_fn(page):
        if sequenced_ids:
            index = sequenced_ids.index(page['_id'])
            #  print(index)
            return index if index >= 0 else 99999999  # TODO

        explicit_index = page.get('selector', {}).get('index', None)
        if explicit_index:
            return int(explicit_index)

        return 0

    return _sort_key_fn


def get_page_ids(vc, book_id, ranges):
    print('getting book graph')
    start_nodes_selector = {"_id": book_id}
    traverse_key_filter_maps_list = [
        {
            "source": {"jsonClass": "ScannedPage"},
            "target": {"jsonClass": "SequenceAnnotation", "canonical": "default_canvas_sequence"}
        }
    ]
    try:
        response = objstore.get_graph(
            vc, start_nodes_selector, traverse_key_filter_maps_list,
            include_incomplete_paths=True, direction='referrer', max_hops=1,
            json_class_projection_map={"*": {"_id": 1, "jsonClass": 1, "selector": 1, "index": 1}}
        )
        print("got book graph")
    except HTTPError as e:
        raise ObjModelException(
            'error in retrieving book graph',
            status_code=e.response.status_code, attachments={"error": str(e)}
        )

    if book_id not in response['graph']:
        raise ObjModelException('book not found/readable', status_code=404)
    if len(response['graph']) == 1:
        raise ObjModelException('book has no readable pages', status_code=404)

    pages = [res for _id, res in response['graph'].items() if res.get('jsonClass') == 'ScannedPage']
    sequences = [res for res in response['graph'].values() if res.get('jsonClass') == 'SequenceAnnotation']

    if sequences or (pages and pages[0].get('selector', {}).get('index', None)):
        pages = sorted(pages, key=get_pages_sort_fn(sequences[0] if sequences else None))

    def _is_in_ranges(index):
        if not ranges:
            return True
        for range in ranges:
            if isinstance(range, int) and range == index:
                return True
            if isinstance(range, list) and len(range) == 2 and index >= range[0] and index < range[1]:
                return True
        return False

    page_ids = [page['_id'] for i, page in enumerate(pages) if _is_in_ranges(i)]
    #  print(page_ids)
    return page_ids


'''
from vedavaapi.client import VedavaapiSession
vs = VedavaapiSession('https://apps.vedavaapi.org/dev/api')
# vs.signin('info@vedavaapi.org', '1234')
vs.access_token = 'cx82sCHX9I3HoMewk5gYOJAqv4XBUDRYs3vj92aqWm'
from vvtextdocs.helpers import importer
txt_doc_id = importer.create_text_document(vs, '5cee571b256c380009d2af7c')
'''
