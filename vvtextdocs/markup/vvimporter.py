import json, sys

from .parser import Element, parse_markup


def elem2txtdoc(elem: Element):
    #  print(elem)
    txtdoc = {
        "jsonClass": "TextSection",
        #  "jsonClassLabel": (elem.ec.metadata or {}).get('label', None) or elem.ec.name,
        "jsonClassLabel": elem.ec.name,
        "metadata": [
            {
                "jsonClass": "MetadataItem",
                "label": k,
                "value": v
            }
            for (k, v) in elem.opening_tag.metadata.items()
        ]
    }
    if not len(txtdoc['metadata']):
        txtdoc.pop('metadata')

    if elem.name():
        txtdoc['name'] = elem.name()

    return txtdoc


def get_sequence_obj_template(parent_id, label, _id):
    return {
        "jsonClass": "Sequence",
        "source": parent_id,
        "jsonClassLabel": label,
        "_id": _id,
        "selector": {"jsonClass": "QualitativeSelector"}
    }


def get_seq_members_resolver_updates(blank_id_uid_map, seqs_members_map):
    updates_graph = {}
    for seq_blank_id, seq_members_blank_ids in seqs_members_map.items():
        seq_id = blank_id_uid_map[seq_blank_id]
        members_ids = [blank_id_uid_map[bid] for bid in seq_members_blank_ids]
        seq_update = {"_id": seq_id, "jsonClass": "Sequence", "_order": members_ids}
        updates_graph[seq_id] = seq_update

    return updates_graph


def marshal_to_vv_graph(parsed_data, root_node_id):

    document = parsed_data['document']  # type: Element

    def elem_tree2vv_graph(
            elem: Element, path, parent_id, relation, rn_id=None,
            vv_graph=None, seqs_members_map=None, reached_ids_map=None):

        vv_graph = vv_graph if vv_graph is not None else {}
        seqs_members_map = seqs_members_map if seqs_members_map is not None else {}
        reached_ids_map = reached_ids_map if reached_ids_map is not None else {}

        txtdoc = elem2txtdoc(elem)
        root_text_doc_graph_id = '_:{}'.format('/'.join(path)) if not rn_id else rn_id
        txtdoc['_id'] = root_text_doc_graph_id
        if txtdoc['jsonClassLabel'] == 'document':
            txtdoc.update({
                "jsonClass": "TextDocument",
                "content": None,
                "components": None  # NOTE
            })
        if relation:
            pass  # NOTE
            txtdoc['relation_to_parent'] = relation

        if (elem.ec.metadata or {}).get('is_analysis', None) == 'true':
            txtdoc['jsonClass'] = 'Annotation'
            link_attr = 'target'
        else:
            link_attr = 'source'
        if parent_id:
            txtdoc[link_attr] = parent_id

        vv_graph[root_text_doc_graph_id] = txtdoc

        if elem.ec.name == '_':
            content_component = {"jsonClass": "Text", "chars": elem.content}
            if 'origin' in elem.opening_tag.metadata:
                content_component['origin'] = elem.opening_tag.metadata['origin']
            txtdoc['content'] = elem.content
            txtdoc['components'] = [content_component]

        else:
            seq_members = []
            seq_blank_id = '_:{}_seq'.format(root_text_doc_graph_id)

            for rel_type in ('content', 'attrs'):
                colln = getattr(elem, rel_type, [])

                if rel_type == 'content':
                    if len(colln) and False not in [child_elem.ec.name == '_' for child_elem in colln]:
                        txtdoc['content'] = ''
                        txtdoc['components'] = []
                        is_in_size_limit = len(colln) < 5000
                        for child_elem in colln:
                            if is_in_size_limit:
                                content_component = {"jsonClass": "Text", "chars": child_elem.content}
                                if 'origin' in child_elem.opening_tag.metadata:
                                    content_component['origin'] = child_elem.opening_tag.metadata['origin']
                                txtdoc['components'].append(content_component)
                            txtdoc['content'] += child_elem.content
                        continue

                for i, child_elem in enumerate(colln):
                    branch_vv_graph, child_blank_id, branch_seqs_members_map, branch_reached_ids_map = elem_tree2vv_graph(
                        child_elem, path + [rel_type, str(i)], seq_blank_id, rel_type,
                        vv_graph=vv_graph, seqs_members_map=seqs_members_map, reached_ids_map=reached_ids_map,
                    )

                    seq_members.append(child_blank_id)

            if len(seq_members):
                seq = get_sequence_obj_template(
                    root_text_doc_graph_id, 'ContentSequence', seq_blank_id)
                vv_graph[seq['_id']] = seq
                reached_ids_map[root_text_doc_graph_id] = [seq['_id']]
                reached_ids_map[seq['_id']] = seq_members
                seqs_members_map[seq_blank_id] = seq_members

        return vv_graph, root_text_doc_graph_id, seqs_members_map, reached_ids_map

    vv_graph, root_text_doc_graph_id, seqs_members_map, reached_ids_map = elem_tree2vv_graph(
        document, ['document'], None, None, rn_id=root_node_id)
    return vv_graph, root_text_doc_graph_id, seqs_members_map, reached_ids_map


def parse_markup_to_graph(root_node_id, markup_document):
    parsed_data = parse_markup(markup_document)
    vv_graph, root_text_doc_graph_id, seq_anno_graph, reached_ids_map = marshal_to_vv_graph(parsed_data, root_node_id)
    vv_graph[root_text_doc_graph_id]['ec_graph_markup'] = parsed_data['ec_graph'].graph_markup
    return vv_graph, root_text_doc_graph_id, seq_anno_graph, reached_ids_map


def paginate_text_docs_graph(vv_graph, reached_ids_map: dict, root_text_doc_graph_id, batch_count=500):
    stack = []
    stack_indices = []

    def _trace_back_nearest_next():
        present_top = stack.pop()
        present_index = stack_indices.pop()
        if not len(stack):
            return None
        parent_id = stack[-1]
        parent_reached_ids = reached_ids_map.get(parent_id)
        if parent_reached_ids is None or len(parent_reached_ids) == present_index + 1:
            return _trace_back_nearest_next()
        sibling_id = parent_reached_ids[present_index + 1]
        sibling = vv_graph[sibling_id]
        stack.append(sibling_id)
        stack_indices.append(present_index + 1)
        return sibling

    def _compute_next():
        if not len(stack):
            stack.append(root_text_doc_graph_id)
            stack_indices.append(0)
            return vv_graph[root_text_doc_graph_id]
        present_top = stack[-1]
        reached_ids = reached_ids_map.get(present_top)
        if reached_ids is not None and len(reached_ids):
            next_id = reached_ids[0]
            next_node = vv_graph[next_id]
            stack.append(next_id)
            stack_indices.append(0)
            return next_node
        else:
            return _trace_back_nearest_next()

    batch = []
    while True:
        if len(batch) >= batch_count:
            yield batch
            batch = []
        next_node = _compute_next()
        if next_node is None:
            if len(batch):
                yield batch
            break
        batch.append(next_node)


def import_from_markup_paginated(vc, markup, root_node_id, update_state=None):

    update_state = update_state or (lambda *args, **kwargs: None)
    update_state(state='PROGRESS', meta={'status': "parsing markup"})
    vv_graph, root_text_doc_graph_id, seqs_members_map, reached_ids_map = parse_markup_to_graph(
        root_node_id, markup)
    #  print(len(vv_graph), len(markup), root_text_doc_graph_id, root_node_id)

    markup_oold_blank_id = '_:markup'
    markup_ool_data = {
        "jsonClass": "DataSet",
        "namespace": "_vedavaapi",
        "identifier": "markup.html",
        "_id": markup_oold_blank_id,
        "source": '_VV:{}'.format(root_text_doc_graph_id) if root_text_doc_graph_id.startswith('_:') else root_text_doc_graph_id
    }
    oold_graph = {"_:markup": markup_ool_data}
    files = [("markup.html", markup)]

    gid_to_uid_map = {}
    def _get_resolved_graph(page):
        page_graph = {}
        for node in page:
            node_copy = node.copy()
            page_graph[node['_id']] = node_copy

            link = 'source' if 'source' in node else ('target' if 'target' in node else None)
            if not link:
                continue
            link_vals = node[link]
            resolved_link_vals = (
                gid_to_uid_map.get(link_vals, link_vals) if isinstance(link_vals, str)
                else [gid_to_uid_map.get(v, v) for v in link_vals]
            )
            #  node[link] = resolved_link_vals  # !NOTE: DON'T DO THIS< as we require blank_ids uniformly for other purposes
            node_copy[link] = resolved_link_vals
        return page_graph

    pages = paginate_text_docs_graph(vv_graph, reached_ids_map, root_text_doc_graph_id)
    oold_attached = False
    count = 0
    from vedavaapi.client import objstore
    for i, page in enumerate(pages):
        #  print(i)
        page_graph = _get_resolved_graph(page)
        should_attach_oold = not oold_attached and root_text_doc_graph_id in page_graph
        #  print(list(page_graph.keys()), should_attach_oold)
        if should_attach_oold:
            oold_attached = True
            page_graph[root_text_doc_graph_id]['markup'] = {
                "jsonClass": "DataRepresentation",
                "data": "_OOLD:{}".format(markup_oold_blank_id)
            }
        update_state(state='PROGRESS', meta={'status': 'posting textdoc hierarchy', 'current': count, 'total': len(vv_graph)})
        response = objstore.post_graph(
            vc, page_graph, ool_data_graph=oold_graph if should_attach_oold else None,
            files=files if should_attach_oold else None, should_return_resources=False
        )
        count += len(page)
        gid_to_uid_map.update(response['graph'])

    root_text_doc_id = gid_to_uid_map[root_text_doc_graph_id]
    seqs_update_graph = get_seq_members_resolver_updates(gid_to_uid_map, seqs_members_map)
    update_response = objstore.post_graph(vc, seqs_update_graph, should_return_resources=False)
    return root_text_doc_id, vv_graph, gid_to_uid_map, seqs_members_map


def import_from_markup(vc, markup, root_node_id, update_state=None):
    update_state = update_state or (lambda *args, **kwargs: None)
    print('parsing markup  ', len(markup))
    update_state(state='PROGRESS', meta={'status': "parsing markup"})
    vv_graph, root_text_doc_graph_id, seqs_members_map, reached_ids_map = parse_markup_to_graph(
        root_node_id, markup)

    markup_oold_blank_id = '_:markup'
    markup_ool_data = {
        "jsonClass": "DataSet",
        "namespace": "_vedavaapi",
        "identifier": "markup.txt",
        "_id": markup_oold_blank_id,
        "source": '_VV:{}'.format(root_text_doc_graph_id) if root_text_doc_graph_id.startswith('_:') else root_text_doc_graph_id
    }
    oold_graph = {"_:markup": markup_ool_data}
    files = [("markup.txt", markup)]

    vv_graph[root_text_doc_graph_id]['markup'] = {
        "jsonClass": "DataRepresentation",
        "data": "_OOLD:{}".format(markup_oold_blank_id)
    }

    from vedavaapi.client import objstore
    update_state(state='PROGRESS', meta={'status': 'posting textdoc hierarchy'})
    response = objstore.post_graph(vc, vv_graph, ool_data_graph=oold_graph, files=files, should_return_resources=False, should_return_oold_resources=False)
    #  print(response)
    root_text_doc_id = response['graph'][root_text_doc_graph_id]

    seqs_update_graph = get_seq_members_resolver_updates(response['graph'], seqs_members_map)
    update_response = objstore.post_graph(vc, seqs_update_graph, should_return_resources=False)
    return root_text_doc_id, vv_graph, response['graph'], seqs_members_map


def delete_existing_children(vc, root_text_doc_id, update_state=None):

    delete_request_data = {
        "filter_doc": json.dumps({"jsonClass": {"$in": ["Sequence", "TextDocument"]}})
    }
    children_delete_response = vc.delete(
        'objstore/v1/resources/{}/specific_resources'.format(root_text_doc_id), data=delete_request_data)

    update_state = update_state or (lambda *args, **kwargs: None)

    if children_delete_response.status_code != 200:
        update_state(state='FAILURE', meta={
            "exc_type": "ApiError",
            "exc_message": ["error in deleting existing image regions"],
        })
        return False

    else:
        response_json = children_delete_response.json()
        if response_json.get('delete_status') and False in response_json.get('delete_status').values():
            update_state(state='FAILURE', meta={
                "exc_type": "PolicyError",
                "exc_message": ['cannot delete all existing regions'],
            })
            return False

    return True
