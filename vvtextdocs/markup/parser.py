import json
import re
from .pyparsing_helper import make_vv_tags

PP_OPENING_TAG, PP_CLOSING_TAAG = make_vv_tags()

class ElementClass(object):

    def __init__(self, name, metadata=None):
        self.name = name
        self.child_of = set()
        self.attr_of = set()
        self.parallels = set()
        self.metadata = metadata or {}

    def update_child_of(self, *args):
        self.child_of.update(args)

    def update_attr_of(self, *args):
        self.attr_of.update(args)

    def update_parallels(self, *args):
        self.parallels.update(args)

    def json(self):
        return {
            "name": self.name,
            "child_of": [p.name for p in self.child_of],
            "attr_of": [p.name for p in self.attr_of],
            "parallels": [pl.name for pl in self.parallels],
            "metadata": self.metadata or {}
        }

    def label(self):
        return self.metadata.get('label', self.name)

    def __str__(self):
        return '<ElementClass {}>'.format(self.name)

    def __repr__(self):
        return str(self)


class ElementClassGraph(object):

    CHILD_OF = 'child_of'
    ATTR_OF = 'attr_of'
    RELATIONS = (CHILD_OF, ATTR_OF)

    def __init__(self):
        self.graph = {}
        self.graph_markup = None

    def get(self, name):
        return self.graph.get(name, None)

    def create(self, name, metadata=None, upsert=False):
        if name not in self.graph or upsert:
            self.graph[name] = ElementClass(name)

        if metadata:
            self.graph[name].metadata = metadata
        return self.graph[name]  # type: ElementClass

    @classmethod
    def compute_relation(cls, ec: ElementClass, ec_hierarchy: list):
        #  print('compute_relation: ', ec, ec_hierarchy, ec.attr_of, ec.child_of)
        if ec.name == '_':
            return 1 if ec_hierarchy[0].name == '_' else 0, cls.CHILD_OF

        for l, ref_ec in enumerate(ec_hierarchy):
            if ref_ec in ec.attr_of:
                return l, cls.ATTR_OF
            elif ref_ec in ec.child_of:
                return l, cls.CHILD_OF
        return None, None


class ElementOpeningTag(object):

    opening_tag_re = re.compile(r'^\[(?P<tag>[^\s/\[\]]+)\s*(?P<info>[^\[\]]*)\]$')
    #  info_re = re.compile(r'\s*(?P<k>[^=\s]+)\s*=\s*"(?P<v>[^"\[\]]*)"\s*')
    info_re = re.compile(r'\s*(?P<k>[^=\s]+)\s*=\s*(?P<v>[^\s\[\]]*)\s*')

    def __init__(self, chars):
        self.chars = chars
        #  print(chars)
        try:
            match = PP_OPENING_TAG.parseString(chars)
            match_dict = match.asDict()
        except ValueError:
            raise ValueError('not an opening tag')
        except Exception as e:
            # pyparsing related error
            raise ValueError('error in parsing opening tag: ' + str(e) + '\n' + 'chars: ' + self.chars)
        '''
        match = self.opening_tag_re.fullmatch(chars)
        if not match:
            raise ValueError('not an opening tag')
        '''
        self.tag = match_dict['tag']
        self.metadata = match_dict['attrs'] or {}


class ElementClosingTag(object):

    closing_tag_re = re.compile(r'^\[/(?P<tag>[^\[\]]+)\]$')

    def __init__(self, chars):
        self.chars = chars
        match = self.closing_tag_re.fullmatch(chars)
        if not match:
            raise ValueError('not a closing tag')
        self.tag = match.group('tag')


class Element(object):

    def __init__(self, ec: ElementClass, ot: ElementOpeningTag, token_index, hierarchy: list, relation):
        self.ec = ec
        self.opening_tag = ot
        self.token_index = token_index
        self.hierarchy = [self] + hierarchy
        self.relation = relation

        self.content = [] if self.ec.name != '_' else ''
        self.attr_map = {}

    def json(self):
        json_doc = {
            "cls": self.ec.name,
            "content": [se.json() for se in self.content if len(se.content)] if isinstance(self.content, list) else self.content,
            "metadata": self.opening_tag.metadata,
            "relation_to_parent": self.relation
        }
        for k in ("metadata", "content"):
            if not isinstance(json_doc[k], str) and not len(json_doc[k]):
                json_doc.pop(k, None)

        return json_doc

    def text(self):
        if isinstance(self.content, str):
            return self.content
        return ''.join([child.text() for child in self.content])

    def name(self):
        name = self.opening_tag.metadata.get('name')
        if name is None and self.attr_map.get('name'):
            name_attr_elem = self.attr_map['name'][0]  # type: Element
            name = name_attr_elem.text()
        return name

    def __str__(self):
        path_parts = [p.ec.name for p in self.hierarchy]
        hash_parts = [str(hash(p) % 10000) for p in self.hierarchy]
        path_parts.reverse()
        hash_parts.reverse()
        return '<Element ec:{} path:{} hp:{}>'.format(self.ec.name, '/'.join(path_parts), '/'.join(hash_parts))

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __hash__(self):
        hash_parts = [hash(p) for p in self.hierarchy[1:]]
        hash_parts.append((self.ec, self.token_index))
        return hash(tuple(hash_parts))


class Token(object):

    tokens_re = re.compile(r'(\[.*?\]|[^\[]*)')
    opening_tag_re = re.compile(r'^\[([^/\[\]].*?)\]$')
    closing_tag_re = re.compile(r'^\[/(.*?)\]$')

    TXT_TOKEN = 'text'
    OPENING_TAG_TOKEN = 'opening_tag'
    CLOSING_TAG_TOKEN = 'closing_tag'

    TOKEN_TYPES = (TXT_TOKEN, OPENING_TAG_TOKEN, CLOSING_TAG_TOKEN)

    def __init__(self, text):
        self.chars = text
        self._classify_token()

    def _classify_token(self):
        ot_match = self.opening_tag_re.fullmatch(self.chars)
        if ot_match:
            self.token_type = self.OPENING_TAG_TOKEN
            return
        ct_match = self.closing_tag_re.fullmatch(self.chars)
        if ct_match:
            self.token_type = self.CLOSING_TAG_TOKEN
            return
        self.token_type = self.TXT_TOKEN

    def __str__(self):
        return self.chars

    def __repr__(self):
        return self.chars


def get_directives_and_document(markup):
    directives_document_re = r'(\s*\[vv_markup\](?P<directives>.*)\[/vv_markup\]\s*)?(?P<document>.*)'
    #  print(markup)
    match = re.fullmatch(directives_document_re, markup, re.DOTALL)

    directives_content = match.group('directives')
    document_content = match.group('document')

    return directives_content, document_content


def parse_directives_to_ec_graph(directives_content):
    ec_graph = ElementClassGraph()
    ec_graph.graph_markup = directives_content
    ec_graph.create('document')
    ec_graph.create('_')

    if not directives_content:
        return ec_graph

    str_tokens = Token.tokens_re.findall(directives_content)
    tokens = [Token(st) for st in str_tokens]
    directive_tokens = [token for token in tokens if token.token_type == Token.OPENING_TAG_TOKEN]

    for directive_token in directive_tokens:
        directive_tag = ElementOpeningTag(directive_token.chars)
        ec1_name = directive_tag.tag

        parent_ec_names = directive_tag.metadata.pop('parents', None)
        if 'child_of' in directive_tag.metadata:
            parent_ec_names = directive_tag.metadata.pop('child_of', None)
        attr_ec_names = directive_tag.metadata.pop('attrs', None)
        attr_of_ec_names = directive_tag.metadata.pop('attr_of', None)
        parallel_ec_names = directive_tag.metadata.pop('parallels', None)

        ec1 = ec_graph.create(ec1_name, metadata=directive_tag.metadata)  # type: ElementClass

        if attr_ec_names:
            for ec_name in attr_ec_names.split(','):
                ec = ec_graph.create(ec_name)  # type: ElementClass
                ec.update_attr_of(ec1)

        if attr_of_ec_names:
            for ec_name in attr_of_ec_names.split(','):
                ec = ec_graph.create(ec_name)
                ec1.update_attr_of(ec)

        if parent_ec_names:
            for ec_name in parent_ec_names.split(','):
                ec = ec_graph.create(ec_name)
                ec1.update_child_of(ec)

        if parallel_ec_names:
            for ec_name in parallel_ec_names.split(','):
                ec = ec_graph.create(ec_name)
                ec1.update_parallels(ec)
                ec.update_parallels(ec1)

    return ec_graph


def pre_process_document_content(document):
    from lxml import etree
    document_elem = etree.HTML(document)
    if document_elem is None:
        return ''
    text_anno_elems = document_elem.findall(".//span[@class='vv-text-anno']")
    for elem in text_anno_elems:
        elem_id = elem.get('id', None)
        if not elem_id:
            continue
        elem.text = '[_ origin={}]'.format(elem_id) + (elem.text or '')

    for br in document_elem.xpath("*//br"):
        br.tail = "\n" + br.tail if br.tail else "\n"

    return etree.tounicode(document_elem, method='text')


def get_dom_tree(ec_graph: ElementClassGraph, document):

    document_elem = Element(ec_graph.get('document'), ElementOpeningTag('[document]'), -1, [], None)
    active_elems = {document_elem}

    str_tokens = Token.tokens_re.findall(document)
    tokens = [Token(st) for st in str_tokens]

    def remove_implicit_elem_if_is_whitespace(elem: Element):
        if elem.ec.name != '_':
            return
        immediate_parent = elem.hierarchy[1]
        if re.fullmatch('^\n+$', elem.content, re.DOTALL) and elem in immediate_parent.content:  # TODO why 2nd cond?
            immediate_parent.content.remove(elem)

    def _push_up_active_elem(elem: Element, parent):
        remove_implicit_elem_if_is_whitespace(elem)
        active_elems.remove(elem)
        for _aelem in list(active_elems):
            if parent in _aelem.hierarchy:
                return
        active_elems.add(parent)

    last_text_anno_id = None

    for token_index, token in enumerate(tokens):
        ##  print('\n\n', active_elems)

        if token.token_type == Token.TXT_TOKEN:
            ##  print(Token.TXT_TOKEN)
            for aelem in list(active_elems):
                if aelem.ec.name == '_':
                    aelem.content += token.chars.replace('&#91;', '[')
                else:
                    impliciti_opening_tag = '[_]' if not last_text_anno_id else '[_ origin={}]'.format(last_text_anno_id)
                    blank_elem = Element(
                        ec_graph.get('_'), ElementOpeningTag(impliciti_opening_tag), token_index,
                        aelem.hierarchy, ElementClassGraph.CHILD_OF
                    )
                    blank_elem.content += token.chars.replace('&#91;', '[')
                    aelem.content.append(blank_elem)
                    active_elems.remove(aelem)
                    active_elems.add(blank_elem)

        elif token.token_type == Token.CLOSING_TAG_TOKEN:
            closing_tag = ElementClosingTag(token.chars)
            ##  print(Token.CLOSING_TAG_TOKEN, closing_tag.tag)
            for aelem in list(active_elems):
                for l, _pelem in enumerate(aelem.hierarchy):
                    if _pelem.ec.name == closing_tag.tag:
                        _push_up_active_elem(aelem, aelem.hierarchy[l + 1])

        elif token.token_type == Token.OPENING_TAG_TOKEN:
            opening_tag = ElementOpeningTag(token.chars)
            ##  print(Token.OPENING_TAG_TOKEN, opening_tag.tag)
            new_elem_ec = ec_graph.get(opening_tag.tag)  # type: ElementClass

            if not new_elem_ec:
                continue

            for aelem in list(active_elems):
                aelem_ec_hierarchy = [e.ec for e in aelem.hierarchy]
                to, relation = ElementClassGraph.compute_relation(new_elem_ec, aelem_ec_hierarchy)

                remove_implicit_elem_if_is_whitespace(aelem)
                ##  print(aelem, new_elem_ec, aelem.hierarchy[to] if to is not None else None)

                if to is None or relation is None:
                    continue

                to_elem = aelem.hierarchy[to]
                sibling = aelem.hierarchy[to - 1] if len(aelem.hierarchy) > 1 else None
                is_parallel_to_sibling = sibling and sibling.ec in new_elem_ec.parallels

                new_elem = Element(new_elem_ec, opening_tag, token_index, to_elem.hierarchy, relation)
                if new_elem not in active_elems:
                    active_elems.add(new_elem)
                    if relation == ElementClassGraph.CHILD_OF:
                        to_elem.content.append(new_elem)
                    elif relation == ElementClassGraph.ATTR_OF:
                        to_elem.content.append(new_elem)  # NOTE
                        attr_type = new_elem.opening_tag.metadata.get('attr', '_')
                        to_elem.attr_map[attr_type] = to_elem.attr_map.get(attr_type, [])
                        to_elem.attr_map[attr_type].append(new_elem)

                if not is_parallel_to_sibling:
                    active_elems.discard(aelem)

                if opening_tag.tag == '_' and 'origin' in opening_tag.metadata:
                    last_text_anno_id = opening_tag.metadata['origin']

    return document_elem

def _get_non_tag_brace_regex(ec_graph):
    tag_comp_regex = r''
    for ec_name in ec_graph.graph.keys():
        tag_comp_regex += r'|/?{}[ \]]'.format(ec_name)
    if not len(tag_comp_regex):
        return r'\['
    tag_comp_regex = tag_comp_regex[1:]
    return r'\[(?!{})'.format(tag_comp_regex)


def parse_markup(markup):
    markup = pre_process_document_content(markup)
    directive_content, document_content = get_directives_and_document(markup)

    ec_graph = parse_directives_to_ec_graph(directive_content)
    document_content = re.sub(_get_non_tag_brace_regex(ec_graph), '&#91;', document_content)
    ##  print(ec_graph, document_content)

    for ec in ec_graph.graph.values():
        #  print("name: ", ec.name, ", ", "child_of: ", ec.child_of, ', ', "attr_of: ", ec.attr_of, ', ', "parallels: ", ec.parallels, '\n\n')
        pass
    document_elem = get_dom_tree(ec_graph, document_content)

    return {"document": document_elem, "ec_graph": ec_graph}


if __name__ == '__main__':
    import sys
    file_path = sys.argv[1]
    stream = open(file_path)

    parsed_data = parse_markup(stream.read())
    output = {
        "document": parsed_data['document'].json(),
        "ec_graph": dict((name, ec.json()) for name, ec in parsed_data['ec_graph'].graph.items())
    }
    print('\n\n====================\n')
    print(json.dumps(output, ensure_ascii=False))
