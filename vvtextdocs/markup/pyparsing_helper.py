from pyparsing import *


unicode_printables = ''.join(chr(c) for c in range(65536) if not chr(c).isspace())
unicode_all = ''.join(chr(c) for c in range(65536) if chr(c) not in '[]')


def make_vv_tags():

    suppress_OB = Suppress(Literal("["))
    suppress_CB = Suppress(Literal("]"))

    tagStr = Regex('[a-zA-Z_0-9]+')
    resname = tagStr.name

    tagAttrName = Word(alphas, alphanums + "_-:")
    tagAttrValue = quotedString.copy().setParseAction(removeQuotes) | Word(unicode_printables, excludeChars="]")
    openTag = (suppress_OB
               + tagStr("tag")
               + Dict(ZeroOrMore(Group(tagAttrName.setParseAction(downcaseTokens)
                                       + Optional(Suppress("=") + tagAttrValue))))('attrs')
               #  + Optional("/", default=[False])("empty").setParseAction(lambda s, l, t: t[0] == '/')
               + suppress_CB)
    closeTag = Combine(Literal("[/") + tagStr + Literal("]"), adjacent=False)

    openTag.setName("[%s]" % resname)
    # add start<tagname> results name in parse action now that ungrouped names are not reported at two levels
    openTag.addParseAction(lambda t: t.__setitem__("start" + "".join(resname.replace(":", " ").title().split()), t.copy()))
    closeTag = closeTag("end").setName("[/%s]" % resname)
    openTag.tag = resname
    closeTag.tag = resname
    openTag.tag_body = SkipTo(closeTag())
    return openTag, closeTag
