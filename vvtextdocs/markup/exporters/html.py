import re
import uuid

from .. import parser
from lxml import etree

def elem_to_html(elem: parser.Element):
    html_elem_tag = 'span' if isinstance(elem.content, str) else 'div'
    is_implicit = elem.ec.name == '_'
    html_elem_cls = 'vvm-ec-{}'.format('_anno' if is_implicit else elem.ec.name)
    if not is_implicit:
        html_elem_cls += ' vvm-ec'
    if not is_implicit:
        if elem.relation == parser.ElementClassGraph.ATTR_OF:
            html_elem_cls += ' vvm-rel-attr_of'

    elem_name = elem.name()

    html_elem = etree.Element(html_elem_tag, attrib={
        "class": html_elem_cls,
        "id": str(hash(elem)),
        "data-ec": elem.ec.name,
        "data-ec-label": elem.ec.metadata.get('label', elem.ec.name),
        "title": ' / '.join([
            '{}{}'.format(m.ec.label(), (' - {}'.format(m.name()) if m.name() else ''))
            for m in reversed(elem.hierarchy)
        ])
    })
    html_elem.attrib.update(
        dict(('data-{}'.format(k), v) for k, v in elem.opening_tag.metadata.items()))

    if not len(elem.content):
        return html_elem

    if isinstance(elem.content, str):
        html_elem.text = elem.content
        return html_elem

    name_child_wrapper = etree.SubElement(html_elem, 'p', attrib={
        "class": 'vvm-elem-name-wrapper'
    })
    name_child = etree.SubElement(name_child_wrapper, 'span', attrib={
        "class": "vvm-elem-name"
    })

    #  elem_name = elem_name or elem.ec.metadata.get('label', elem.ec.name)
    if elem_name is not None:
        name_child.text = '{} - {}'.format(
            elem.ec.metadata.get('label', elem.ec.name), elem_name or '')

    children_wrapper = etree.SubElement(html_elem, 'div', attrib={
        "class": 'vvm-elem-children'
    })

    child_parallels = []
    current_child_parallel = None
    prev_elem = None

    for child_elem in elem.content:
        if prev_elem and (prev_elem.ec == child_elem.ec):
            branch = current_child_parallel[prev_elem.ec.name]
            branch.append(child_elem)
            prev_elem = child_elem
            continue
        if prev_elem and (child_elem.ec in prev_elem.ec.parallels):
            current_child_parallel[child_elem.ec.name] = current_child_parallel.get(child_elem.ec.name, [])
            branch = current_child_parallel[child_elem.ec.name]
            branch.append(child_elem)
            prev_elem = child_elem
            continue
        current_child_parallel = {}
        child_parallels.append(current_child_parallel)
        current_child_parallel[child_elem.ec.name] = [child_elem]
        prev_elem = child_elem

    def extend_children(_root_html_elem, _child_branch, has_parallels):
        if not len(_child_branch):
            return False
        branch_ec_class = _child_branch[0].ec.name
        is_implicit_branch = branch_ec_class == '_'
        if is_implicit_branch:
            wrapper_elem = etree.SubElement(_root_html_elem, 'span', attrib={
                "class": 'vvm-ec vvm-ec-_',
                "data-ec": '_',
                "id": uuid.uuid4().hex
            })
        else:
            wrapper_elem = _root_html_elem
        for i, child in enumerate(_child_branch):
            child_html_elem = elem_to_html(child)
            wrapper_elem.append(child_html_elem)
            if not isinstance(child.content, str):
                continue
            if i == len(branch) - 1:
                child_html_elem.text = re.sub(r'\n$', '', child_html_elem.text or '', flags=re.M)
                #  print(child_html_elem.text)
            if not child_html_elem.text:
                wrapper_elem.remove(child_html_elem)
        return is_implicit_branch

    single_implicit_child = False
    for cp_count, child_parallel in enumerate(child_parallels):
        if not len(child_parallel):
            continue
        elif len(child_parallel) == 1:
            branch = list(child_parallel.values())[0]
            single_implicit_child = extend_children(children_wrapper, branch, False) and len(child_parallels) == 1
        else:
            cp_elem = etree.SubElement(children_wrapper, 'div', {
                "class": "vvm-parallels",
                "id": 'vvm-cps-{}-{}'.format(
                    str(hash(html_elem)), cp_count
                )
            })
            for branch_count, ec_name in enumerate(sorted(child_parallel.keys())):
                branch = child_parallel[ec_name]
                branch_elem = etree.SubElement(cp_elem, 'div', {
                    "class": "vvm-parallel-branch",
                    "id": "vvm-cpb-{}-{}-{}".format(
                        str(hash(html_elem)), cp_count, branch_count
                    )
                })
                title_elem = etree.SubElement(branch_elem, 'h3', {
                    "class": "vvm-parallel-branch-title"
                })
                title_elem.text = branch[0].ec.metadata.get('label', branch[0].ec.name)
                extend_children(branch_elem, branch, True)

    if single_implicit_child and not elem_name:
        implicit_child = html_elem[1][0]
        for child_elem in html_elem.getchildren():
            html_elem.remove(child_elem)
        #  html_elem.clear()
        html_elem.extend(implicit_child.iterchildren())

    return html_elem


def markup_to_html(markup):
    parse_result = parser.parse_markup(markup)
    document_elem = parse_result['document']  # type: parser.Element
    ec_graph = parse_result['ec_graph']  # type: parser.ElementClassGraph

    document_html_elem = etree.Element('html')
    body_elem = etree.SubElement(document_html_elem, 'body')
    body_elem.append(elem_to_html(document_elem))

    return body_elem


if __name__ == '__main__':
    import sys
    file_path = sys.argv[1]
    stream = open(file_path)

    html = markup_to_html(stream.read())
    print(etree.tounicode(html, method=True))



'''
from vvtextdocs.markup.exporters import html
from lxml import etree

cont = open('/home/damodarreddy/పొందినవి/గురుకులం/vedavaapi/text_document_handler/vvtextdocs/adms_.vvm').read()
he = html.markup_to_html(cont)
etree.tounicode(he)
'''