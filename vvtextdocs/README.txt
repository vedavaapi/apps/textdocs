VV_client must have the following classes:
ScannedBook: [ScannedPages]*

ScannedPage: tree(HOCR_PAGE) [ImageRegion]*
- Methods:
  compute_text()

Separate services:
Segmenter:
- Methods:
    /book_segments/<vv_book_url> ?page_indexes=....
        Populates layout info as children of ScannedPage objects
    /page_segments/<vv_page_url>
        Populates layout info as children of ScannedPage objects

ImageRegion: [TextAnnotation|ChoiceAnnotation]

TextDocument: markedup_text markup_desc origin:ScannedBook [TextSection]*
/textdocs/<id>/
- Methods:
    export_as_html() (GET /textdocs/<id>/html)
    export_as_json() (GET /textdocs/<id>/json)
    export_as_markup() (GET /textdocs/<id>/markup)
    import_from_markup(markedup_text) 
        (POST /textdocs/from_markup ?from_markup=markedup_text)
    import_from_scannedbook(scanned_book_id, page_indexes)
        (POST /textdocs/from_scanned_book ?scanned_book_id=... from_page_indexes=...)
    children()

TextSection: derived class of TextDocument
    classlabel 
    label 
    metadata 
    markup_tag
    [ text_content | [TextSection]* ] 
    source:[TextDocument|TextSection] 
    relation_to_source:[child_of|attr_of]
