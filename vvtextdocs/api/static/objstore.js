async function getGraph({ kContext, start_nodes_selector, traverse_key_filter_maps_list, direction, max_hops, include_incomplete_paths, json_class_projection_map }) {
    // let stringifyIfDefined = (doc) => (doc !== undefined) ? JSON.stringify(doc) : undefined;
    /* let purgeUnDefined = (doc) => {
        Object.keys(doc).forEach((k) => {
            if (doc[k] === undefined) {
                delete doc[k];
            }
        });
        return doc;
    } */
    let data = {
        start_nodes_selector, traverse_key_filter_maps_list, direction, max_hops, include_incomplete_paths, json_class_projection_map
    };
    let formData = new FormData();
    Object.keys(data).forEach((k) => {
        let val = data[k];
        if (val === undefined) {
            return;
        }
        val = typeof (val) === 'string' ? val : JSON.stringify(val);
        formData.append(k, val);
    });

    const graphUrl = `${kContext.site}/objstore/v1/graph`;
    const headers = {};
    if (kContext.vc.accessToken) {
        headers['Authorization'] = `Bearer ${kContext.vc.accessToken}`;
    }
    return fetch(graphUrl, {
        method: 'PUT',
        body: formData,
        // @ts-ignore
        headers,
        credentials: "include"
    }).then(checkStatus).then(jsonify);
}

async function postGraph({ kContext, graph, should_return_resources, upsert }) {
    if (!kContext.vc.accessToken) {
        alert('not authorized!');
        return;
    }
    let body = new FormData();
    let postData = {
        graph: JSON.stringify(graph),
        should_return_resources: String(Boolean(should_return_resources)),
        upsert: String(Boolean(upsert))
    };
    Object.keys(postData).forEach((k) => {
        body.append(k, postData[k]);
    });

    const graphUrl = `${kContext.siteContext.vvSiteUrl}/objstore/v1/graph`;
    const headers = {};
    if (kContext.vc.accessToken) {
        headers['Authorization'] = `Bearer ${kContext.vc.accessToken}`;
    }
    return fetch(graphUrl, {
        method: 'POST',
        body,
        // @ts-ignore
        headers,
    }).then(checkStatus).then(jsonify);
}
