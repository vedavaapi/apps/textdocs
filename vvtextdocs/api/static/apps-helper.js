function AppsHelper(kContext, handledTypes, apis) {
    this.kContext = kContext;
    this.apps = [];
    this.handledTypes = handledTypes;
    this.apis = apis;
}

AppsHelper.prototype = {
    get() {
        // console.log({ getContext: this.kContext.siteContext, url: this.kContext.siteContext.vvSiteUrl });
        let resourcesUrl = new URL(`${this.kContext.site}/objstore/v1/resources`);
        let selectorDoc = {
            jsonClass: 'Service',
            handledTypes: { "$in": this.handledTypes },
        };
        if (this.apis) {
            selectorDoc.apis = { "$in": this.apis };
        }
        resourcesUrl.searchParams.set('selector_doc', (JSON.stringify(selectorDoc)));
        let headers = new Headers();
        if (this.kContext.vc.accessToken) {
            headers.append('Authorization', `Bearer ${this.kContext.vc.accessToken}`);
        }

        return fetch(resourcesUrl.href, {
            headers,
            mode: 'cors',
        }).then(checkStatus).then(jsonify)
            .then((response) => {
                console.log({ response });
                this.apps.length = 0;
                this.apps.push(...response.items);
                return this.apps;
            });
    },

    getMatchedApps(jsonClass, api) {
        let matchedApps = [];
        for (let app of this.apps) {
            if (!app.handledTypes || !app.handledTypes.includes(jsonClass)) {
                continue;
            }
            if (api) {
                if (!app.apis || !app.apis.includes(api)) {
                    continue
                }
            }
            else {
                if (app.apis) {
                    continue;
                }
            }
            matchedApps.push(app);
        }
        return matchedApps;
    },

    getPreferredApp(jsonClass, api) {
        let matchedApps = this.getMatchedApps(jsonClass, api);
        if (matchedApps.length == 0) {
            return null;
        }
        return matchedApps[0];
    },

    getAppUrl(app, objId, jsonClass, customIdKey = null) {
        if (!app) {
            return null;
        }
        let appUrl = new URL(app.url);
        appUrl.searchParams.set('kaveri', this.kContext.url);
        if (objId) {
            appUrl.searchParams.set('_id', objId);
            if (customIdKey) {
                appUrl.searchParams.set(customIdKey, objId);
            }
        }
        if (jsonClass) {
            appUrl.searchParams.set('jsonClass', jsonClass);
        }
        return appUrl.href;
    },

    launch(app, objId, jsonClass, customIdKey = null) {
        if (!app) {
            return;
        }
        let appUrl = this.getAppUrl(app, objId, jsonClass, customIdKey);
        window.open(appUrl, '_blank');
    }
}
