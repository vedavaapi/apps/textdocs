// @ts-nocheck
'use strict';

function getHeaders(kContext) {
    const headers = {};
    if (kContext.vc && kContext.vc.accessToken) {
        headers.Authorization = `Bearer ${kContext.vc.accessToken}`;
    }
    return headers;
}


let TextDocumentEditor = function TextDocumentEditor(config, scb) {
    this.config = config;
    //  this.editorContainer = document.getElementById(config.editorContainerId);
    this.textDocId = this.config.textDocId;
    this.statusCell = document.getElementById(this.config.statusCellId);
    this.kContext = this.config.kContext;

    tinymce.init({
        selector: `#${this.config.containerId}`,
        plugins: "code",
        /*inline: true,
        skin: 'dark',
        width: 600,
        height: 300,*/

      })
    .then((resp) => {
        console.log({resp});
        this.editor = resp[0];
        if(scb) {
            scb(this);
        }
    });
    /*BalloonEditor
        .create(this.editorContainer)
        .then((editor) => {this.editor = editor})
        .catch( error => {
            console.log( error );
        } );*/
    // CKEDITOR.replace(this.config.editorContainer);
};


/*
TextDocumentEditor.prototype.setDocument = function(textDoc) {
    this.textDoc = textDoc;
    if(!textDoc) {
        // this.editor.setData('');
    }
    else {
        console.log({editor: this.editor});
        // tinymce.get(this.editorContainer).getBody().innerHTML = textDoc.markup || '';
        this.editor.getBody().innerHTML = textDoc.markup || '';
        // this.editor.setData(this.textDoc.markup || '');
    }
};
*/

TextDocumentEditor.prototype.render = function() {
    this.editor.getBody().innerHTML = this.markup || '';
};

TextDocumentEditor.prototype.getDocument = function() {
    return this.editor.getContent();
};

TextDocumentEditor.prototype.getMarkup = function() {
    if(!this.textDocId) {
        return Promise.resolve(null);
    }
    let markupUrl = `../${this.textDocId}/content`;
    let params = new URLSearchParams({
        site_url: this.kContext.site,
        format: 'markup'
    });
    if(this.kContext.vc.accessToken) {
        params.append('access_token', this.kContext.vc.accessToken);
    }
    return fetch(`${markupUrl}?${params.toString()}`, {
        method: 'GET',
    }).then(checkStatus).then(jsonify)
    .then((response) => {
        //  console.log({response});
        this.textDoc = response['text_doc'];
        this.markup = response['markup'];
        return response
    });
};

TextDocumentEditor.prototype.init = function() {
    this.statusCell.innerText = 'retrieving textdoc markup....';
    return this.getMarkup().then(() => {
        this.statusCell.innerText = 'textdoc markup retrieved successfully';
        this.render();
        return true;
    }).catch((e) => {
        this.statusCell.innerHTML = 'error in retrieving textdoc markup';
        return false;
    });
};

TextDocumentEditor.prototype.updateTaskStatus = function(url) {
    return fetch(url).then(checkStatus).then(jsonify).then((response) => {
        console.log(response);
        let state = response.state;
        let status = response.status;
        let current = response.current;
        let total = response.total;

        let statusString = `state:${state.toLowerCase()}; `;
        if (status) {
            statusString += ` ${status}; `;
        }
        if (current !== undefined && total !== undefined) {
            statusString += `  ${current}/${total}`;
        }

        this.statusCell.innerText = statusString;
        if (response.state === 'SUCCESS') {
            console.log(response);
            let result = response['result'];
            this.textDocId = result['root_text_doc_id'];
            return result;
        } else if (response.state === 'PENDING' || response.state === 'PROGRESS') {
            return new Promise((resolve, reject) => {
                setTimeout(() => {resolve(this.updateTaskStatus(url))}, 2000);
            });
        } else {
            throw new Error(response.status);
        }
    });
}

TextDocumentEditor.prototype.persist = function() {
    if(!this.kContext.vc.accessToken) {
        alert('not authorized !');
        return;
    }
    let formData = new FormData();
    formData.append('markup', this.getDocument());
    formData.append('site_url', this.kContext.site);
    formData.append('access_token', this.kContext.vc.accessToken);
    formData.append('attach_html', 'true');
    if(this.textDocId) {
        formData.append('root_text_doc_id', this.textDocId);
    }

    this.statusCell.innerHTML = `saving TextDocument....  ${loadingSvg}`;
    return fetch('../from_markup_task', {
        method: 'POST',
        body: formData
    }).then(checkStatus).then(jsonify)
    .then((result) => {
        // this.statusCell.innerText = `Text Documnet ${result['root_text_doc_id']} created successfully`;
        // this.textDocId = result['root_text_doc_id'];
        // return result;
        return this.updateTaskStatus(result.status_url);
    }).catch((resp) => {
        resp.json().then((rj) => {
            this.statusCell.innerText = 'error: ' + rj.error;
        });
    });

};



let TextDocHTMLViewer = function(config) {
    this.config = config;
    this.container = document.getElementById(this.config.containerId);
    this.statusCell = document.getElementById(this.config.statusCellId);
    this.kContext = config.kContext;
    this.textDocId = this.config.textDocId;
    this.selectedNode = null;
    this.selectedVVMNode = null;
    // this.miniViewDiv = document.getElementById(this.config.miniViewDivId);
    this.selElemViewDiv = document.getElementById(this.config.selElemViewId);
    this.originsDiv = document.getElementById(this.config.originsDivId);
    this.originToggleButton = document.getElementById(this.config.originToggleButtonId);
    this.originViewDiv = document.getElementById(this.config.originViewDivId);
    this.originToggleButton.addEventListener('click', (event) => {
        if(!this.selectedNode) {
            return;
        }
        this.originViewDiv.style.display = 'block';
        this.showOrigin(this.selectedNode);
    })
};


TextDocHTMLViewer.prototype.getHtml = function() {
    let apiUrl = `../${this.textDocId}/content`;
    let params = new URLSearchParams({
        "site_url": this.kContext.site,
        "format": "html"
    });
    if(this.kContext.vc.accessToken) {
        params.append('access_token', this.kContext.vc.accessToken);
    }
    return fetch(`${apiUrl}?${params.toString()}`, {
        method: 'GET',
    }).then(checkStatus).then(jsonify)
    .then((result) => {
        this.textDoc = result['text_doc'];
        this.html = result['html'];
        return result;
    });
};

TextDocHTMLViewer.prototype.render = function() {
    if(!this.html) {
        return;
    }
    this.container.innerHTML = this.html;
    let textDocElem = document.getElementsByClassName('vvm-ec-document')[0];
    textDocElem.firstChild.firstChild.innerText = this.textDoc.title || '';
    this.tabifyParallels();
    this.makeCollapsable();
    // this.setupMiniView();
    this.makeContentEditable();
};

TextDocHTMLViewer.prototype.tabifyParallels = function() {
    let parallelWrappers = document.getElementsByClassName('vvm-parallels');
    //  console.log({parallelWrappers});
    for(let pw of parallelWrappers) {
        tinytabs(pw, {
            anchor: false,
            hideTitle: true,
            sectionClass: "vvm-parallel-branch",
            tabsClass: "vvm-parallel-tabs",
            tabClass: "vvm-parallel-tab",
            titleClass: "vvm-parallel-branch-title",
            selClass: "vvm-parallel-sel"
        });
    }
};

TextDocHTMLViewer.prototype.toggleChildrenVisibility = function(nameElem) {
    let toggleIconElem = nameElem.firstChild;
    let isExpanded = toggleIconElem.innerText == '▼';
    let childrenWrapper = nameElem.nextSibling;
    if(isExpanded) {
        childrenWrapper.style.display = 'none';
        toggleIconElem.innerText = '►';
        toggleIconElem.classList.add('vvm-children-toggle-none');
    }
    else {
        childrenWrapper.style.display = 'block';
        toggleIconElem.innerText = '▼';
        toggleIconElem.classList.remove('vvm-children-toggle-none');
    }
};

TextDocHTMLViewer.prototype.makeCollapsable = function() {
    let namewrappers = document.getElementsByClassName('vvm-elem-name-wrapper');
    let _this = this;
    let toggle = function (event) {
        let target = this;
        _this.toggleChildrenVisibility(target);
    };
    let select = function(event) {
        let target = this;
        //  window.selTarget = target;
        // _this.unSetSelection();
        // _this.updateSelElemView(target.parentElement);
        _this.onVVMElementSelect(target.parentElement);
    };
    let onMouseEnter = function(event) {
        let target = this;
        let childrenWrapper = target.nextSibling;
        childrenWrapper.classList.add('vvm-elem-children-hover');
        if(target.children[1]) {
            target.children[1].classList.add('vvm-elem-name-hover');
        }
    };
    let onMouseLeave = function(event) {
        let target = this;
        let childrenWrapper = target.nextSibling;
        childrenWrapper.classList.remove('vvm-elem-children-hover');
        if(target.children[1]) {
            target.children[1].classList.remove('vvm-elem-name-hover');
        }
    }
    for(let nw of namewrappers) {
        let toggleIconElem = document.createElement('span');
        toggleIconElem.classList.add('vvm-children-toggle');
        toggleIconElem.innerText = '▼';
        nw.prepend(toggleIconElem);
        nw.addEventListener('click', toggle);
        nw.addEventListener('click', select);
        nw.addEventListener('mouseenter', onMouseEnter);
        nw.addEventListener('mouseleave', onMouseLeave);
    }
};

TextDocHTMLViewer.prototype.getVVMElementHierarchy = function(leafElem) {
    let hierarchy = [];
    if(leafElem.classList.contains('vvm-ec')) {
        hierarchy.push(leafElem);
    }
    if(!leafElem.classList.contains('vvm-ec-document')) {
        hierarchy = this.getVVMElementHierarchy(leafElem.parentElement).concat(hierarchy);
    }
    return hierarchy;
};

TextDocHTMLViewer.prototype.vvmElementName = function(elem) {
    let nwElem = elem.firstChild;
    if(!nwElem.classList.contains('vvm-elem-name-wrapper')) {
        return null;
    }
    let nameElem = nwElem.children[1];
    if(!nameElem || !nameElem.classList.contains('vvm-elem-name')) {
        return null;
    }
    return nameElem.innerText;
};

TextDocHTMLViewer.prototype.updateSelElemView = function(selElem) {
    this.selElemViewDiv.innerText = '';
    let hierarchy = this.getVVMElementHierarchy(selElem);
    if(!hierarchy.length) {
        return false;
    }
    let leafElem = hierarchy[hierarchy.length - 1];
    this.selectedVVMNode = leafElem;
    let info = {
        name: this.vvmElementName(leafElem) || leafElem.getAttribute('data-ec-label') || leafElem.getAttribute('data-ec'),
        type: leafElem.getAttribute('data-ec-label') || leafElem.getAttribute('data-ec'),
        index: Array.prototype.indexOf.call(leafElem.parentElement.children, leafElem),
        _id: `<a href="${this.kContext.url + '/vihari/res/'+ (leafElem.id)}" target="_blank">${leafElem.id}</a>`
    };
    if(selElem.parentElement === leafElem) {
        info.componentIndex = Array.prototype.indexOf.call(leafElem.children, selElem);
        hierarchy.push(selElem);
    }
    let dl = document.createElement('dl');
    dl.classList.add('sel-elem-info-list')
    this.selElemViewDiv.append(dl);
    Object.keys(info).forEach((k) => {
        let v = info[k];
        let dt = document.createElement('dt');
        dt.classList.add('sel-elem-info-item-key');
        dt.innerText = k + ': ';
        let dd = document.createElement('dd');
        dd.classList.add('sel-elem-info-item-val');
        dd.innerHTML = v;
        dl.append(dt);
        dl.append(dd);
    });
    let pathElem = document.createElement('p');
    this.selElemViewDiv.append(pathElem);
    pathElem.classList.add('sel-elem-view-path');
    let pathLableElem = document.createElement('span');
    pathLableElem.innerText = 'path: ';
    pathLableElem.classList.add('sel-elem-view-path-lable');
    pathElem.appendChild(pathLableElem);
    let onClick = (event) => {
        let linkedElemId = event.target.getAttribute('data-id');
        if(!linkedElemId) {
            return;
        }
        let linkedElem = document.getElementById(linkedElemId);
        if(!linkedElem) {
            return;
        }
        this.setCursorSelection(linkedElem);
        this.setSelection();
    };
    hierarchy.forEach((m, i) => {
        let a = document.createElement('a');
        a.classList.add('sel-elem-view-hierarchy-elem');
        a.href = 'javascript:void(0);';
        a.innerText = m != selElem ? this.vvmElementName(m) || m.getAttribute('data-ec-label') || m.getAttribute('data-ec') : '.....';
        a.setAttribute('data-id', m.id);
        a.addEventListener('click', onClick);
        pathElem.append(a);
        if(i < hierarchy.length - 1) {
            let seperator = document.createElement('span');
            seperator.classList.add('sel-elem-view-hierarchy-seperator');
            seperator.innerText = ' » ';
            pathElem.append(seperator);
        }
    });
    return true;
};

TextDocHTMLViewer.prototype.removeClass = function(cls) {
    let clsElems = document.getElementsByClassName(cls);
    for(let elem of clsElems) {
        elem.classList.remove(cls);
    }
}

TextDocHTMLViewer.prototype.unHighlightPrevious = function() {
    this.removeClass('vvm-ec-selected');
    this.removeClass('vvm-ec-selected-immediate');
    this.removeClass('vvm-ec-selected-immediate-inline');
}

TextDocHTMLViewer.prototype.getImmediateVVMElement = function(elem) {
    let goUp = (_elem) => {
        if(_elem.classList.contains('vvm-ec')) {
            return _elem;
        }
        return goUp(_elem.parentElement);
    }
    return goUp(elem);
}

TextDocHTMLViewer.prototype.highlightVVMElement = function(elem) {
    elem.classList.add('vvm-ec-selected');
    let immediateVVMElement = this.getImmediateVVMElement(elem);
    let isInlineElem = immediateVVMElement.tagName === 'SPAN'
    if (isInlineElem) {
        isInlineElem = !Boolean(immediateVVMElement.getElementsByTagName('br').length);
    }
    immediateVVMElement.classList.add(isInlineElem ? 'vvm-ec-selected-immediate-inline' : 'vvm-ec-selected-immediate');
};

TextDocHTMLViewer.prototype.updateOriginDetailsVisibility = function() {
    this.originsDiv.innerText = '';
    this.originViewDiv.style.display = 'none';
    this.originToggleButton.style.display = this.selectedNode.getAttribute('data-origin') ? 'block' : 'none';
}

TextDocHTMLViewer.prototype.onVVMElementSelect = function(elem) {
    let selectedNode = this.getSelectionStart();
    this.selectedNode = selectedNode;
    this.updateSelElemView(elem);
    this.unSetSelection();
    this.highlightVVMElement(elem);
    this.updateOriginDetailsVisibility();

    let event = new CustomEvent('selchange', {
        detail: {
            elem,
            vvmElem: this.getImmediateVVMElement(elem),
            hierarchy: this.getVVMElementHierarchy(elem),
        }
    });
    this.container.dispatchEvent(event);
};

TextDocHTMLViewer.prototype.unSetSelection = function() {
    this.unHighlightPrevious();
    // this.selectedNode = null;
};

TextDocHTMLViewer.prototype.setCursorSelection = function(elem) {
    elem = elem.childNodes ? elem.childNodes[0] : elem;
    let range, selection;
    if (window.getSelection && document.createRange) {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(elem);
        selection.removeAllRanges();
        selection.addRange(range);
    } else if (document.selection && document.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(elem);
        range.select();
    }
};

TextDocHTMLViewer.prototype.setSelection = function() {
    let selectedNode = this.getSelectionStart();
    if(selectedNode == this.selectedNode) {
        return;
    }
    this.unSetSelection();
    this.selectedNode = selectedNode;
    this.onVVMElementSelect(this.selectedNode);
}

TextDocHTMLViewer.prototype.getSelectionStart = function() {
    let node = window.getSelection().anchorNode;
    return (node.nodeType == 3 ? node.parentNode : node);
};

TextDocHTMLViewer.prototype.makeContentEditable = function() {
    let textDocElem = document.getElementsByClassName('vvm-ec-document')[0];
    textDocElem.contentEditable = true;
    textDocElem.spellcheck = false;
    textDocElem.addEventListener('click', (e) => {
        //  console.log('document onclick');
        //  window.e = e;
        if(e.target.classList.contains('vvm-elem-name-wrapper') || e.target.parentElement.classList.contains('vvm-elem-name-wrapper')) {
            //  console.log(e.target, false);
            return;
        }
        //  console.log(e.target, true);
        this.setSelection();
    });
    let namewrappers = document.getElementsByClassName('vvm-elem-name-wrapper');
    let tabNavs = document.getElementsByClassName('vvm-parallel-tabs');
    let makeUnEditable = (colln) => {
        for (let elem of colln) {
            elem.contentEditable = false;
        }
    }
    makeUnEditable(namewrappers);
    makeUnEditable(tabNavs);
    let editCallback = (e) => {
        let keyCode = Number(e.keyCode);
        if(![37, 38, 39, 40].includes(keyCode)) {
            e.preventDefault();
        }
        if(keyCode == 79 && e.ctrlKey) {
            let selectionStartNode = this.getSelectionStart();
            this.originViewDiv.style.display = 'block';
            this.showOrigin(selectionStartNode);
            return;
        }
        setTimeout(() => {
            this.setSelection();
        }, 0);
    };
    textDocElem.addEventListener('keydown', editCallback);
};

TextDocHTMLViewer.prototype.collapseAll = function() {
    let namewrappers = document.getElementsByClassName('vvm-elem-name-wrapper');
    for(let nw of namewrappers) {
            let toggleIconElem = nw.firstChild;
            let isExpanded = toggleIconElem.innerText == '▼';
            if(isExpanded) {
                this.toggleChildrenVisibility(nw);
            }
    }
};

TextDocHTMLViewer.prototype.expandAll = function() {
    let namewrappers = document.getElementsByClassName('vvm-elem-name-wrapper');
    for(let nw of namewrappers) {
            let toggleIconElem = nw.firstChild;
            let isExpanded = toggleIconElem.innerText == '▼';
            if(!isExpanded) {
                this.toggleChildrenVisibility(nw);
            }
    }
};

TextDocHTMLViewer.prototype._getOriginComponent = function(text, origin, statusText) {
    let wrapper = document.createElement('div');
    wrapper.classList.add('origin-wrapper');

    let status = document.createElement('p');
    status.classList.add('origin-status');
    status.innerText = statusText;
    wrapper.append(status);

    if(!text || !origin) {
        return wrapper;
    }

    let textWrapper = document.createElement('p');
    textWrapper.classList.add('origin-text-wrapper');
    let textLabel = document.createElement('span');
    textLabel.innerText = 'text: ';
    textLabel.classList.add('origin-text-label');
    let textElem = document.createElement('span');
    textElem.classList.add('origin-text');
    textElem.innerText = text ? text : '';
    textWrapper.append(textLabel);
    textWrapper.append(textElem);
    wrapper.append(textWrapper);

    let imageWrapper = document.createElement('div');
    imageWrapper.classList.add('origin-image-wrapper');
    wrapper.append(imageWrapper);
    return wrapper;
};

TextDocHTMLViewer.prototype.showOrigin = function(elem) {
    this.originsDiv.innerText = '';
    let originId = elem.getAttribute('data-origin');
    if(!originId) {
        let comp = this._getOriginComponent(null, null, 'no origin found');
        this.originsDiv.append(comp);
        return;
    }
    let text = elem.innerText;
    let comp = this._getOriginComponent(text, originId, 'loading image...');
    this.originsDiv.append(comp);
    let statusCell = comp.firstChild;
    let imageWrapper = comp.children[2];

    let annoGraphUrl = `${this.kContext.site}/objstore/v1/graph`;
    let formData = new FormData();
    let data = {
        "start_nodes_selector": {_id: originId},
        "traverse_key_filter_maps_list": [{target: {jsonClass: 'ImageRegion'}}, {source: {jsonClass: 'ScannedPage'}}],
        "json_class_projection_map": {
            ScannedPage: {_id: 1, source: 1, representations: 1, jsonClass: 1},
            ImageRegion: {_id: 1, jsonClass: 1, source: 1, "selector.default": 1},
            TextAnnotation: {_id: 1, jsonClass: 1, target: 1}
        },
        "max_hops": 2,
        "include_incomplete_paths": true,
        'direction': 'referred'
    };
    Object.keys(data).forEach((k) => {
        let v = data[k];
        if(typeof(v) !== 'string') {
            v = JSON.stringify(v);
        }
        formData.append(k, v);
    });
    fetch(annoGraphUrl, {
        method: 'PUT',
        body: formData,
        headers: getHeaders(this.kContext),
        mode: 'cors',
    }).then(checkStatus).then(jsonify).then((resp) => {
        console.log({resp});
        let graph = resp.graph;
        let anno = graph[originId];
        if(!anno) {
            statusCell.innerText = 'invalid origin id';
            return false;
        }
        let regionId = Array.isArray(anno.target) ? anno.target[0] : anno.target;
        let region = graph[regionId];
        if(!region || !region.selector.default) {
            statusCell.innerText = 'region does not exist for given origin';
            return false;
        }
        let pageId = Array.isArray(region.source) ? region.source[0] : region.source;
        let page = graph[pageId];
        if(!page) {
            statusCell.innerText = 'page does not exist for given origin';
            return;
        }
        let ooldData = (page.representations.stillImage || [{}])[0].data;
        if(!ooldData) {
            statusCell.innerText = 'ooldata description not exist for given page';
            return;
        }
        let ooldId = ooldData.slice(6);
        let rectSelector = region.selector.default;
        let rectSelectorValueRegex = new RegExp('xywh=([0-9]+) *, *([0-9]+) *, *([0-9]+) *, *([0-9]+)');
        let valueParts = rectSelector.value.match(rectSelectorValueRegex);
        let [, x, y, w, h] = valueParts;
        let snippetUrl = `${this.kContext.site}/iiif_image/v1/objstore/${ooldId}/${x},${y},${w},${h}/full/0/default.jpg`;
        console.log({snippetUrl});
        let image = document.createElement('img');
        image.src = snippetUrl;
        image.classList.add('origin-image');
        imageWrapper.append(image);
        statusCell.innerText = 'image retrieved';
    }).catch((e) => {
        statusCell.innerText = 'error in retrieving graph';
        console.log({e});
        Promise.reject(e);
    });
};

TextDocHTMLViewer.prototype.updateHTML = function(html, textDoc) {
    this.statusCell.innerText = 'retrieving html view....';
    return Promise.resolve(true).then(() => {
        this.statusCell.innerHTML = 'HTML retrieved successfully';
        if(textDoc) {
            this.textDoc = textDoc;
        }
        this.html = html;
        return true;
    }).then(() => {
        this.render();
        return true;
    });
};

TextDocHTMLViewer.prototype.init = function() {
    this.statusCell.innerText = 'retrieving html view....';
    return this.getHtml().then(() => {
        this.statusCell.innerHTML = 'HTML retrieved successfully';
        return true;
    }).catch((e) => {
        this.statusCell.innerHTML = 'error in retrieving HTML';
        console.log(e);
        return false;
    }).then(() => {
        this.render();
        return true;
    });
};


let TextDocMetaViewer = function(config) {
    this.config = config;
    this.kContext = this.config.kContext;
    this.textDoc = config.textDoc;
    this.container = document.getElementById(this.config.containerId);
};


TextDocMetaViewer.prototype = {
    render() {
        let table = document.createElement('table');
        table.classList.add('meta-table');
        let props = {
            _id: `<a href="${this.kContext.url + '/vihari/res/' +(this.textDoc._id)}" target="_blank">${this.textDoc._id}</a>`,
            parent: this.textDoc.source ? `<a href="${this.kContext.url + '/vihari/res/' +(this.textDoc.source)}" target="_blank">${this.textDoc.source}</a>` : this.textDoc.source,
            title: this.textDoc.title,
            description: this.textDoc.description
        };
        Object.keys(props).forEach((k) => {
            if(!props[k]) {
                return;
            }
            let tr = document.createElement('tr');
            tr.classList.add('meta-row');
            tr.innerHTML = `<td class="meta-key-cell">${k}</td><td class="meta-val-cell">${props[k]}</td>`;
            table.appendChild(tr);
        });
        this.container.appendChild(table);
    }
}


function annoDeleteActionCb(event) {
    let target = event.target;
    let parent = target.parentElement;
    let annoId = parent.getAttribute('data-anno-id');

}


class AnnotationComponent {
    constructor(config) {
        this.config = config;
        this.kContext = this.config.kContext;
        this.annoEditor = config.annoEditor;
        this.anno = config.anno;
        this.schema = this.annoEditor.schemasGraph[this.anno.jsonClass];
        this.classLabel = this.anno.jsonClassLabel || (this.schema ? this.schema.label || this.schema.name : this.anno.jsonClass);
        this.referred = config.referred;
        this.element = undefined;
    }

    elemId(id) {
        return `cnt-anno-elem-${id}`;
    }

    render() {
        if (this.element !== undefined) {
            return this.element;
        }
        if (!this.anno) {
            return null;
        }
        /**
         * @type: HTMLElement
         */
        this.element = document.createElement('div');
        this.element.id = this.elemId(this.anno._id);
        this.element.innerHTML = `
            <span class="cnt-anno-jc">${this.classLabel}: </span>
            <br>
            <span class="cnt-anno-date">on ${new Date(this.anno.created).toLocaleDateString()}</span>
            <span class="cnt-anno-actions" data-anno-id="${this.anno._id}" ${this.referred ? 'data-referred-id="' + this.referred + '"' : ""}>
                <a class="cnt-anno-action" href="${this.kContext.url + '/vihari/res/' +(this.anno._id)}" target="_blank">🛈</a>
                <span class="cnt-anno-action" title="edit" data-action="edit">✍</span>
                <span class="cnt-anno-action" title="delete" data-action="delete">✖</span>
            </span>
            <span class="cnt-anno-action-status" style="display:none;"></span>
            <br>`;
        this.element.classList.add('cnt-anno');
        return this.element;
    }

}

class DefaultAnnoComponent extends AnnotationComponent {
    render() {
        super.render();
        if (!this.element) {
            return this.element;
        }
        let jsonDiv = document.createElement('div');
        jsonDiv.classList.add('default-anno-body-elem');
        jsonDiv.innerText = JSON.stringify(this.anno.body, undefined, 2);
        this.element.appendChild(jsonDiv);
        return this.element;
    }
}


AnnotationComponent.registry = {
    Annotation: DefaultAnnoComponent,
};

AnnotationComponent.make = function(config) {
    const schemasGraph = (config.annoEditor || {}).schemasGraph || {};
    const jsonClass = config.anno.jsonClass;
    let cls = AnnotationComponent.registry[jsonClass];

    if (!cls) {
        let parentJsonClass = jsonClass;
        for(let i = 0; i < Object.keys(schemasGraph).length; i += 1) {
            if (!parentJsonClass) {
                break;
            }
            if(AnnotationComponent.registry[parentJsonClass]) {
                break;
            }
            let schema = schemasGraph[parentJsonClass];
            if (!schema) {
                break;
            }
            parentJsonClass = schema.source;
        }
        let parentCls = AnnotationComponent.registry[parentJsonClass];
        cls = parentCls || AnnotationComponent.registry.Annotation;
    }

    /**
     * @type: AnnotationComponent
     */
    const comp = new cls(config);
    return comp;
}


class TextAnnotationComponent extends AnnotationComponent {
    render() {
        super.render();
        if (!this.element  || !this.anno.body) {
            return this.element;
        }
        let textElement = document.createElement('div');
        textElement.classList.add('cnt-anno-text');
        textElement.innerText = this.anno.body[0].chars;
        this.element.appendChild(textElement);
        return this.element;
    }
}


class ChoiceItemComponent {
    constructor(config) {
        this.config = config;
        this.anno = config.anno;
        this.kContext = config.kContext;
        this.choiceAnno = config.choiceAnno;
        this.element = null;
        this.annoComponent = AnnotationComponent.make(config);
    }

    render() {
        if (this.element) {
            return this.element;
        }
        if (!this.anno) {
            return this.element;
        }

        this.choice = ((this.choiceAnno.choices || {})[this.anno._id]) || {};
        this.upVoters = this.choice.up_voters || [];
        this.downVoters = this.choice.down_voters || [];
        this.castedVote = null;
        if (this.kContext.session.user) {
            let userId = this.kContext.session.user._id;
            this.castedVote = this.upVoters.includes(userId) ? 'up' : (this.downVoters.includes(userId) ? 'down' : null);
        }

        this.element = document.createElement('div');
        this.element.classList.add('cnt-anno-choice-item-wrapper');
        this.element.appendChild(this.annoComponent.render());

        let annoActionsWrapper = document.createElement('div');
        annoActionsWrapper.classList.add('cnt-anno-action-wrapper');
        annoActionsWrapper.setAttribute('data-anno-id', this.anno._id);
        annoActionsWrapper.innerHTML = `
            <div class="cnt-anno-choice-action-vote ${this.castedVote === 'up' ? 'cnt-anno-choice-vote-casted' : ''}" data-vote="up">
                <span class="vote-icon glyphicon glyphicon-thumbs-up"></span><!--br-->
                <span class="vote-count">${this.upVoters.length}</span>
            </div>
            <div class="cnt-anno-choice-action-vote ${this.castedVote === 'down' ? 'cnt-anno-choice-vote-casted' : ''}" data-vote="down">
                <span class="vote-icon glyphicon glyphicon-thumbs-down"></span><!--br-->
                <span class="vote-count">${this.downVoters.length}</span>
            </div>
        `;
        this.element.appendChild(annoActionsWrapper);
        return this.element;
    }
}


class ChoiceAnnotationComponent extends AnnotationComponent {
    constructor(config) {
        super(config);
        this.choices = config.choices;
        this.statusElem = null;
        this.wrapperElem = null;
        this.memberComponents = {};
        this.membersSchema = this.anno.membersClass ? this.annoEditor.schemasGraph[this.anno.membersClass] : null;
        this.membersClassLabel = this.membersSchema ? (this.membersSchema.label || this.membersSchema.name) : this.membersClass;
    }

    async handleVote(vote, choiceId, voteBlock) {
        if (!this.kContext.session.user) {
            alert(`error in getting user info. ${!this.kContext.vc.accessToken ? 'not authorized' : ''}`);
            return;
        }
        this.anno.choices = this.anno.choices || {};
        let choices = this.anno.choices;
        let choiceBackup = choices[choiceId];
        let choice = JSON.parse(JSON.stringify(choiceBackup || {}));
        this.anno.choices[choiceId] = choice;

        let userId = this.kContext.session.user._id;
        let voteType = vote === 'up' ? 'up_voters' : 'down_voters';
        let otherVoteType = vote === 'down' ? 'up_voters' : 'down_voters';
        if (!choice[voteType]) {
            choice[voteType] = [];
        }
        if (!choice[otherVoteType]) {
            choice[otherVoteType] = [];
        }
        let voteRecord = choice[voteType];
        let voteIndex = voteRecord.indexOf(userId);
        if (voteIndex === -1) {
            if (choice[otherVoteType] && choice[otherVoteType].includes(userId)) {
                // alert('you cannot both upvote and downvote');
                // return;
                choice[otherVoteType].splice(choice[otherVoteType].indexOf(userId), 1);
            }
            voteRecord.push(userId);
        } else {
            voteRecord.splice(voteIndex, 1);
        }

        let resp;
        this.statusElem.innerText = 'recording vote';
        try {
            resp = await postGraph({
                kContext: this.kContext,
                graph: {
                    [this.anno._id] : this.anno,
                },
                should_return_resources: true,
            });
            this.statusElem.innerText = 'vote casted successfully';
            this.anno = resp.graph[this.anno._id];
            this.renderChoiceItem(this.memberComponents[choiceId].anno);
        } catch(e) {
            this.statusElem.innerText = 'error in recording vote';
            this.anno.choices[choiceId] = choiceBackup;
            console.log(e);
        }
    }

    render() {
        if (!this.anno) {
            this.element = null;
            return this.element;
        }
        this.element = document.createElement('div');
        this.element.id = this.elemId(this.anno._id);
        this.element.innerHTML = `
            <span class="cnt-choice-anno-excl pointable">►</span> 
            <span class="cnt-anno-jc">${this.classLabel}: </span> of <span class="cnt-anno-choice-class-label>">${this.membersClassLabel}</span> 
            <span class="cnt-anno-actions" data-anno-id="${this.anno._id}">
                <a class="cnt-anno-action" href="${this.kContext.url + '/vihari/res/' + (this.anno._id)}" target="_blank">🛈</a>
                <span class="cnt-anno-action" title="add" data-action="annotate">+</span>
                <span class="cnt-anno-action" title="delete" data-action="delete">✖</span>
            </span>
            <br>
        `;

        this.statusElem = document.createElement('span');
        this.statusElem.classList.add('cnt-anno-choice-status');
        this.element.appendChild(this.statusElem);

        this.wrapperElem = document.createElement('div');
        this.wrapperElem.classList.add('cnt-anno-choice-wrapper');
        this.element.appendChild(this.wrapperElem);

        this.wrapperElem.addEventListener('click', (event) => {
            let target = event.target;
            let voteBlock;
            if (target.classList.contains('cnt-anno-choice-action-vote')) {
                voteBlock = target;
            } else if (target.parentElement.classList.contains('cnt-anno-choice-action-vote')) {
                voteBlock = target.parentElement;
            } else {
                return;
            }
            let actionsWrapper = voteBlock.parentElement;
            let annoId = actionsWrapper.getAttribute('data-anno-id');
            let vote = voteBlock.getAttribute('data-vote');
            this.handleVote(vote, annoId, voteBlock);
        });

        this.element.firstElementChild.addEventListener('click', (event) => {
            let isCollapsed = event.target.innerText === '►';
            event.target.innerText = isCollapsed ?'▼' : '►';
            this.wrapperElem.style.display = isCollapsed ? 'block' : 'none';
        });

        this.element.classList.add('cnt-anno');
        if (this.choices) {
            this.setChoices();
        } else {
            this.getChoices();
        }
        return this.element;
    }

    renderChoiceItem(choice) {
        if(!choice) {
            return;
        }
        let cid = choice._id;
        let component = new ChoiceItemComponent({
            kContext: this.kContext,
            anno: choice,
            annoEditor: this.annoEditor,
            referred: this.anno._id,
            choiceAnno: this.anno,
        });
        let elem = component.render();
        if (!this.memberComponents[cid]) {
            this.wrapperElem.appendChild(elem);
        } else {
            this.wrapperElem.replaceChild(elem, this.memberComponents[cid].element);
        }
        this.memberComponents[cid] = component;
    }

    setChoices() {
        if (!this.choices || !this.wrapperElem) {
            return;
        }
        this.wrapperElem.innerText = '';
        this.memberComponents = {};

        Object.keys(this.choices).forEach((cid) => {
            let choice = this.choices[cid];
            this.renderChoiceItem(choice);
        });
        return this.wrapperElem;
    }

    async getChoices() {
        let resp;
        try{
            resp = await getGraph({
                kContext: this.kContext,
                start_nodes_selector: {
                    _id: this.anno._id,
                },
                traverse_key_filter_maps_list: [{
                    target: {},
                }],
                direction: 'referrer',
                max_hops: 1
            });
        } catch(e) {
            this.statusElem.innerText = 'error in retrieving choices';
        }
        this.choices = resp.graph;
        delete this.choices[this.anno._id];
        this.statusElem.innerText = '';
        this.setChoices();
    }

    upsert(anno) {
        this.renderChoiceItem(anno);
    }

    deleteChoice(choiceId) {
        let choiceComponent = this.memberComponents[choiceId];
        if (!choiceComponent) {
            return;
        }
        choiceComponent.element.remove();
        delete this.memberComponents[choiceId];
    }

    initUpdate(annoId) {
        if (!this.kContext.vc.accessToken) {
            alert('not authorized for action');
            return;
        }
        let annoComponent = this.memberComponents[annoId];
        if (!annoComponent) {
            return;
        }
        let anno = annoComponent.anno;
        this.annoEditor.init({
            target: this.anno._id,
            prefill: anno,
        });
    }
}


AnnotationComponent.registry.TextAnnotation = TextAnnotationComponent;
AnnotationComponent.registry.ChoiceAnnotation = ChoiceAnnotationComponent;


let AnnotationsListComponent = function(config) {
    this.config = config;
    this.kContext = config.kContext;
    this.element = config.element;
    this.annoEditor = config.annoEditor;

    this.annoComponents = {};
}


AnnotationsListComponent.prototype = {
    render(annos, vvmElem) {
        this.vvmElem = vvmElem;
        this.element.innerText = '';
        annos.forEach((anno) => {
            let comp = AnnotationComponent.make({
                kContext: this.kContext,
                anno,
                annoEditor: this.annoEditor,
            });
            this.annoComponents[anno._id] = comp;
            this.element.appendChild(comp.render());
        });
    },

    getParentAndElement(detail) {
        let anno = detail.anno;

    },

    upsert(detail) {
        let isChoice = Boolean(detail.choice);
        let res = isChoice ? detail.choice : detail.anno;

        let target = res.target;

        let annoConf = {
            kContext: this.kContext,
            anno: res,
            annoEditor: this.annoEditor,
        };
        if (isChoice) {
            annoConf.choices = [detail.anno];
        }

        let annoComponent = AnnotationComponent.make(annoConf);
        
        let newElement = annoComponent.render();
        if (this.annoComponents[res._id]) {
            this.element.replaceChild(newElement, this.annoComponents[res._id].element);
            this.annoComponents[res._id] = annoComponent;
        } else if (this.annoComponents[target]) {
            this.annoComponents[target].upsert(detail.anno);
        } else {
            this.element.prepend(newElement);
            this.annoComponents[res._id] = annoComponent;
        }
    },
}


let AnnotationsListWrapper = function(config) {
    this.config =config;
    this.kContext = config.kContext;
    this.targetId = config.targetId;
    this.annoEditor = annoEditor;

    this.element = config.element;
    this.statusElement = this.element.firstElementChild;
    this.listElem = this.statusElement.nextElementSibling;

    this.annoListComponent = new AnnotationsListComponent({
        kContext: this.kContext,
        element: this.listElem,
        annoEditor: this.annoEditor,
    });

    this.listElem.addEventListener('click', (event) => {
        let target = event.target;
        console.log({event});
        if (!target.classList.contains('cnt-anno-action')) {
            return;
        }
        let parent = target.parentElement;
        let annoId = parent.getAttribute('data-anno-id');
        let referredId = parent.getAttribute('data-referred-id');
        let action = target.getAttribute('data-action');

        console.log({parent, annoId, action});

        if (action === 'delete') {
            return this.delete(annoId, referredId);
        } else if (action === 'edit') {
            return this.initiateUpdate(annoId, referredId);
        } else if(action === 'annotate') {
            this.annotate(annoId);
        }
    });
}


AnnotationsListWrapper.prototype = {

    render(vvmElem) {
        this.vvmElem = vvmElem;
        this.listElem.innerText = '';
        let resourcesUrl = `${this.kContext.site}/objstore/v1/resources`;
        let params = {
            selector_doc: {
                target: vvmElem.id,
            },
            projection: {
                resolvedPermissions: 0
            },
            sort_doc: [
                ["created", -1]
            ]
        };
        let sParams = new URLSearchParams();
        Object.keys(params).forEach((k) => {
            let v = params[k];
            sParams.append(k, JSON.stringify(v));
        });
        let headers = {};
        if (this.kContext.vc.accessToken) {
            headers.Authorization = `Bearer ${this.kContext.vc.accessToken}`;
        }
        this.statusElement.innerText = 'retrieving annotations....';
        fetch(`${resourcesUrl}?${sParams.toString()}`, {
            headers,
        }).then(checkStatus).then(jsonify).catch((e) => {
            this.statusElement.innerText = 'error in retrieving annotations';
            console.log(e);
        }).then((resp) => {
            this.statusElement.innerText = 'retrieved annotations';
            console.log({resp});
            this.annoListComponent.render(resp.items, this.vvmElem);
        });
    },

    upsert(detail) {
        this.annoListComponent.upsert(detail);
    },

    annotate(annoId) {
        if (!this.kContext.vc.accessToken) {
            alert('not authorized for action');
            return;
        }
        let annoComponent = this.annoListComponent.annoComponents[annoId];
        if (!annoComponent) {
            return;
        }
        let anno = annoComponent.anno;
        let membersClass = anno.membersClass;
        if (!membersClass && annoComponent.memberComponents) {
            let firstMemberComponet = annoComponent.memberComponents[0];
            if (firstMemberComponet) {
                membersClass = firstMemberComponet.anno.jsonClass;
            }
        }
        let context = {
            target: anno._id,
            disableChoice: true,
        };
        if (membersClass) {
            anno.membersClass = membersClass; // NOTE we are updating local copy, but not persisting eagerly.
            context.prefill = {
                jsonClass: membersClass,
            };
        };
        this.annoEditor.init(context);
    },

    deleteElem(annoId, referredId) {
        let component = this.annoListComponent.annoComponents[annoId];
        if (component) {
            component.element.remove();
            delete this.annoListComponent.annoComponents[annoId];
            return;
        }
        let cannoComponent = this.annoListComponent.annoComponents[referredId];
        if (!cannoComponent) {
            return;
        }
        cannoComponent.deleteChoice(annoId);
    },

    delete(annoId, referredId) {
        if (!this.kContext.vc.accessToken) {
            alert('not authorized for action');
            return;
        }
        let url = `${this.kContext.site}/objstore/v1/resources`;
        let headers = {
            Authorization: `Bearer ${this.kContext.vc.accessToken}`,
        };
        let formData = new FormData();
        formData.append('resource_ids', JSON.stringify([annoId]));

        this.statusElement.innerText = `deleteing anno ${annoId}`;
        let annoElemId = AnnotationComponent.prototype.elemId(annoId);
        let annoElem = document.getElementById(annoElemId);
        annoElem.classList.add('cnt-anno-deleting');

        fetch(url, {
            method: 'DELETE',
            body: formData,
            headers,
        }).then(checkStatus).then(jsonify).catch((e) => {
            this.statusElement.innerText = 'error in deleting anno tations' + String(e);
        }).then((res) => {
            let isDeleted = res.delete_status[annoId];
            annoElem.classList.remove('cnt-anno-deleting');
            setTimeout(() => {
                this.statusElement.innerText = '';
            }, 3000);
            if (!isDeleted) {
                this.statusElement.innerText = 'you may not have permission to delete this annotation';
                return;
            }
            this.deleteElem(annoId, referredId);
            this.statusElement.innerText = `deleted ${annoId} successfully`;
        });
    },

    initiateUpdate(annoId, referredId) {
        if (!this.kContext.vc.accessToken) {
            alert('not authorized for action');
            return;
        }
        let annoComponent = this.annoListComponent.annoComponents[annoId];
        if (!annoComponent) {
            if (referredId) {
                let choiceAnno = this.annoListComponent.annoComponents[referredId];
                if (!choiceAnno) {
                    return;
                }
                choiceAnno.initUpdate(annoId);
                return;
            } else {
                return;
            }
        }
        let anno = annoComponent.anno;
        this.annoEditor.init({
            target: this.vvmElem.id,
            prefill: anno,
        });
    }
}



let AnnoCreator = function(config) {
    this.config = config;
    this.element = config.element;
    this.kContext = config.kContext;
    this.annoEditor = config.annoEditor;

    this.plus = this.element.firstElementChild;
    this.statusElem = this.plus.nextElementSibling;

    this.plus.addEventListener('click', () => {
        if (!this.vvmElem) {
            return;
        }
        this.annoEditor.init({
            target: this.vvmElem.id,
        });
    });
}

AnnoCreator.prototype = {

    clear(resetStatus=false) {
        this.element.style.display = 'block';
        if (resetStatus) {
            this.statusElem.innerText = '';
        }
    },

    render(vvmElem) {
        this.vvmElem = vvmElem;
        this.clear(true);
        this.element.style.display = 'block';
    },
}

let AnnoGenerator = function(config) {
    this.config = config;
    this.element = config.element;
    this.kContext = config.kContext;
    this.annoEditor = config.annoEditor;
    /** @type: AnnotationsListWrapper */
    this.annoListWrapper = config.annoListWrapper;
    this.annoListComponent = this.annoListWrapper.annoListComponent;

    this.appsHelper = new AppsHelper(this.kContext, ['*'], ['/morph']);
    this.apps = [];

    this.appsHelper.get().then((apps) => {
        this.apps = apps;
        // this.element.style.display = 'block';
    });

    this.labelBlock = this.element.firstElementChild
    this.statusBlock = this.labelBlock.nextElementSibling;
    this.statusElement = this.statusBlock.firstElementChild;

    this.actionsBlock = this.statusBlock.nextElementSibling;

    this.actionApis = {
        'morph': '/morph',
    };

    this.actionsBlock.addEventListener('click', (event) => {
        let target = event.target;
        let actionElem;
        if (target.classList.contains('anno-generate-action')) {
            actionElem = target;
        } else if(target.parentElement.classList.contains('anno-generate-action')) {
            actionElem = target.parentElement;
        } else {
            return;
        }
        let action = actionElem.getAttribute('data-analysis');
        if (!action || !this.actionApis[action]) {
            return;
        }
        this.generate(action);
    });
}

AnnoGenerator.prototype = {

    render(vvmElem) {
        this.vvmElem = vvmElem;
        if (this.vvmElem && this.apps.length) {
            this.element.style.display = 'block';
            this.statusElement.innerText = '';
        }
    },

    generate(action) {
        if (!this.kContext.vc.accessToken) {
            this.statusElement.innerText = 'not authorized!';
            return;
        }
        let api = this.actionApis[action];
        let app = this.appsHelper.getPreferredApp('*', api);
        if (!app) {
            this.statusElement.innerText = 'no compatible app found';
            return;
        }
        let appUrl = app.url;
        if (String(appUrl).endsWith('/')) {
            appUrl = appUrl.substring(0, appUrl.length - 1);
        }
        let actionUrl = appUrl + api;
        let formData = new FormData();
        formData.append('site_url', this.kContext.site);
        formData.append('target', this.vvmElem.id);
        formData.append('access_token', this.kContext.vc.accessToken);

        this.statusElement.innerText = 'performing analysis';
        let _this = this;
        return fetch(actionUrl, {
            method: 'POST',
            body: formData,
        }).then(checkStatus).catch((r) => {
            r.json().then((rj) => {
                _this.statusElement.innerText = `error: ${rj.message}`;
            });
        }).then(jsonify).then((response) => {
            this.statusElement.innerText = 'analysis success added annos';
            let result = response.result;
            result.forEach((choiceAnnoItems) => {
                let cAnno = choiceAnnoItems.choice_anno;
                let items = choiceAnnoItems.choice_items;
                this.annoListComponent.upsert({
                    choice: cAnno,
                });
                items.forEach((item) => {
                    this.annoListComponent.upsert({
                        choice: cAnno,
                        anno: item,
                    });
                });
            });
        });
    }

}


let SeleElemSummeryComponent = function(config) {
    this.config = config;
    this.kContext = config.kContext;
    this.element = config.element;
}

SeleElemSummeryComponent.prototype = {
    render(vvmElem) {
        this.element.innerHTML = `
            <a class="summery-elem-id" href="${this.kContext.url + '/vihari/res/' + (vvmElem.id)}">${vvmElem.id}</a><br>
            <blockquote class="summery-elem">${vvmElem.innerText}</blockquote>
        `;
    }
}


let AnnoManager = function(config) {
    this.config = config;
    this.kContext = config.kContext;
    this.containerId = config.containerId;
    this.container = document.getElementById(this.containerId);
    this.annoEditor = config.annoEditor;

    this.summeryConatiner = this.container.firstElementChild;
    this.createAnnoContainer = this.summeryConatiner.nextElementSibling;
    if (!this.kContext.vc.accessToken) {
        this.createAnnoContainer.style.display = 'none';
    }

    this.generateAnnoContainer = this.createAnnoContainer.nextElementSibling;

    this.annoListContainer = this.generateAnnoContainer.nextElementSibling;

    this.summeryElem = new SeleElemSummeryComponent({
        kContext: this.kContext,
        element: this.summeryConatiner,
    });
    this.annoListWrapperElem = new AnnotationsListWrapper({
        kContext: this.kContext,
        element: this.annoListContainer,
        annoEditor: this.annoEditor,
    });
    this.creatorComponent = new AnnoCreator({
        kContext: this.kContext,
        element: this.createAnnoContainer,
        annoEditor: this.annoEditor,
    });
    this.generatorComponent = new AnnoGenerator({
        kContext: this.kContext,
        element: this.generateAnnoContainer,
        annoEditor: this.annoEditor,
        annoListWrapper: this.annoListWrapperElem,
    });

    this.annoEditor.rootElem.addEventListener('annoupsert', (event) => {
        this.onUpsert(event);
    })
}

AnnoManager.prototype = {
    onEvent(e) {
        let detail = e.detail;
        let vvmElem = detail.vvmElem;
        this.summeryElem.render(vvmElem);
        this.annoListWrapperElem.render(vvmElem);
        this.creatorComponent.render(vvmElem);
        this.generatorComponent.render(vvmElem);
    },

    onUpsert(event) {
        let detail = event.detail;
        this.annoListWrapperElem.upsert(detail);
    }
}


let AnnoEditor = function(config) {
    this.config = config;
    this.kContext = this.config.kContext;
    this.schemaNames = [];
    this.schemasGraph = {};
    this.schemas = {};

    this.context = {};

    this.rootElem = this.config.rootElem;
    this.ibarElem = this.config.ibarElem;
    this.statusElem = this.config.statusElem;
    this.bodyElem = this.config.bodyElem;
    this.schemaSelectElem = this.config.schemaSelectElem;
    this.asChoiceElem = this.config.asChoiceElem;
    this.formWrapperElem = this.config.formWrapperElem;
    this.toggleElem = this.config.toggleElem;
    this.submitElem = this.config.submitElem;

    this.ibarElem.addEventListener('click', () => {
        this.toggleBodyVisibility();
    });
    this.schemaSelectElem.addEventListener('change', () => {
        if (this.context.impliitJsonClass) {
            delete this.context.impliitJsonClass;
            delete this.context.prefill.jsonClass;
        }
        this.prepareView(this.context);
    });
    this.submitElem.addEventListener('click', async () => {
        let resp;
        this.editor.disable();
        try {
            resp = await this.saveAnno();
        } catch(e) {
            this.editor.enable();
            throw e;
        }
    });
}

AnnoEditor.prototype = {

    toggleBodyVisibility() {
        let v = this.bodyElem.style.display;
        let isVisible = v === 'block';
        this.toggleElem.innerText = isVisible ? '+' : '-'
        this.bodyElem.style.display = isVisible ? 'none' : 'block';
    },

    ensureVisibility(shouldVisible) {
        let isVisible = this.bodyElem.style.display === 'block';
        let shouldToggle = shouldVisible !== isVisible;
        if (shouldToggle) {
            this.toggleBodyVisibility();
        }
    },

    reset() {
        this.statusElem.innerText = '';
        if (this.editor) {
            this.editor.destroy();
        }
    },

    async getSchemas() {
        this.statusElem.innerText = 'retrieving schema names';
        let response;
        try {
            response = await getGraph({
                kContext: this.kContext,
                start_nodes_selector: {
                    jsonClass: 'VVSchema',
                    name: 'Annotation',
                },
                traverse_key_filter_maps_list: [
                    {
                        source: {
                            jsonClass: 'VVSchema'
                        }
                    }
                ],
                max_hops: -1,
                json_class_projection_map: {
                    '*': {
                        name: 1,
                        _id: 1,
                        _reached_ids: 1,
                        source: 1,
                        label: 1,
                    },
                },
                direction: 'referrer',
            });
        } catch(e) {
            console.log(e);
            this.statusElem.innerText = 'error in retrieving schema names';
        }
        this.statusElem.innerText = 'schema names retrieved successfully';
        // console.log({response});
        this.schemasGraph = {};
        this.schemaNames = [];
        Object.keys(response.graph).forEach((_id) => {
            let schema = response.graph[_id];
            let graphItem = {
                name: schema.name,
                _id: schema._id,
                children: ((schema._reached_ids || {}).source || []).map((cid) => response.graph[cid].name),
                source: (response.graph[schema.source] || {}).name,
                label: schema.label,
            };
            this.schemasGraph[schema.name] = graphItem;
            this.schemaNames.push(schema.name);
        })
        this.schemaNames = this.schemaNames.sort();
        this.schemaSelectElem.innerHTML = '';
        this.schemaNames.forEach((sn) => {
            let graphItem = this.schemasGraph[sn];
            let optionElem = document.createElement('option');
            optionElem.value = sn;
            optionElem.innerText = graphItem.label || graphItem.name;
            optionElem.classList.add('edit-schema-names-item');
            if (sn === 'TextAnnotation') {
                optionElem.selected = true;
            }
            this.schemaSelectElem.appendChild(optionElem);
        });
    },

    getDisplaySchema(schema) {
        if (Array.isArray(schema)) {
            return schema.map((v, i) => this.getDisplaySchema(v));
        }
        if (Object.prototype.isPrototypeOf(schema)) {
            if (!Object.prototype.hasOwnProperty.call(schema, 'properties')) {
                let no = {};
                Object.keys(schema).forEach((k) => {
                    no[k] = this.getDisplaySchema(schema[k]);
                });
                if (Object.prototype.hasOwnProperty.call(no, 'enum') && !Object.prototype.hasOwnProperty.call(no, 'type')) {
                    no.type = 'string';
                }
                return no;
            }
            let no = JSON.parse(JSON.stringify(schema, (k, v) => ['properties', 'uniqueItems'].includes(k) ? undefined : v));

            let displayFields = no._display || [];
            let targetIndex = displayFields.indexOf('target');
            if (targetIndex !== -1) {
                displayFields.splice(targetIndex, 1);
            }

            no.required = (no.required || []).filter((f) => displayFields.includes(f));
            no.properties = {};
            Object.keys(schema.properties).forEach((k) => {
                if (!displayFields.includes(k)) {
                    return;
                }
                no.properties[k] = this.getDisplaySchema(schema.properties[k]);
            });
            return no;
        }
        return JSON.parse(JSON.stringify(schema));
    },

    projectObjectToSchema(o, schema) {
        if (!schema) {
            return undefined;
        }
        if (['number', 'string', 'boolean'].includes(typeof (o)) || o === undefined || o === null) {
            return o;
        }
        if (Array.isArray(o)) {
            let itemSchema = schema.items;
            let o2 = o.map((item) => this.projectObjectToSchema(item, itemSchema)).filter((item) => item !== undefined);
            if (o.length && !o2.length) {
                // should check inequality instead;
                return undefined;
            }
            return o2;
        }
        let properties = schema.properties;
        if (!properties) {
            return undefined;
        }
        let o2 = {};
        Object.keys(properties).forEach((k) => {
            if (!Object.prototype.hasOwnProperty.call(o, k)) {
                return;
            }
            let vSchema = properties[k];
            let v = o[k];
            let v2 = this.projectObjectToSchema(v, vSchema);
            if (v2 !== undefined) {
                o2[k] = v2;
            }
        });
        // console.log({o, schema, o2});
        return o2;
    },

    async getSchema(jsonClass) {
        if (Object.prototype.hasOwnProperty.call(this.schemas, jsonClass)) {
            return this.schemas[jsonClass];
        }
        if (!Array.prototype.includes.call(this.schemaNames, jsonClass)) {
            return null;
        }
        let resp;
        try {
            resp = await getGraph({
                kContext: this.kContext,
                start_nodes_selector: {
                    jsonClass: 'VVSchema',
                    name: jsonClass
                },
                direction: 'referrer',
            });
        } catch(e) {
            throw e;
        }
        if (!resp.start_nodes_ids.length) {
            return null;
        }
        let schema = this.getDisplaySchema(resp.graph[resp.start_nodes_ids[0]]);
        this.schemas[schema.name] = schema;
        return schema;
    },

    async init(context) {
        this.reset();
        this.ensureVisibility(true);
        this.prepareView(context);
    },

    async prepareView(context) {
        console.log('in prepare view');
        this.formWrapperElem.style.display = 'none';
        this.context = context || {};

        this.context.prefill = this.context.prefill || {};
        let prefill = this.context.prefill;

        let isUpdate = Object.prototype.hasOwnProperty.call(prefill, '_id')
        this.submitElem.innerText = isUpdate ? 'UPDATE' : 'CREATE';
        this.asChoiceElem.parentElement.style.display = isUpdate || this.context.disableChoice ? 'none' : 'inline-block';

        let jsonClass = prefill.jsonClass || this.schemaSelectElem.value;
        if (!jsonClass || !this.schemaNames.includes(jsonClass)) {
            this.statusElem.innerText = 'invalid jsonClass';
            return;
        }
        this.schemaSelectElem.value = jsonClass;
        this.context.impliitJsonClass = !Boolean(prefill.jsonClass);
        prefill.jsonClass = jsonClass;

        this.statusElem.innerText = 'retrieving json schema.....';
        let schema;
        try {
            schema = await this.getSchema(jsonClass);
            if (schema.name !== jsonClass) {
                // to handle race conditions
                return;
            }
            this.statusElem.innerText = 'schema retrieved successfully';
            this.formWrapperElem.style.display = 'block';
        } catch(e) {
            console.log(e);
            this.statusElem.innerText = 'error in getting schema....';
            return;
        }
        if (this.editor) {
            this.editor.destroy();
        }
        console.log({prefill});
        this.editor = new JSONEditor(this.formWrapperElem, {
            schema,
            theme: 'bootstrap3',
            // startval: prefill,
            startval: this.projectObjectToSchema(prefill, schema),
            iconlib: 'bootstrap3',
            disable_edit_json: true,
            form_name_root: '',
            prompt_before_delete: false,
            object_layout: 'table',
            compact: true,
        });
        // this.editor.setValue(prefill);
    },

    getEditorValue() {
        let editorVal = this.editor.getValue();
        let nv = JSON.parse(JSON.stringify(this.context.prefill));
        
        let merge = (o1, o2) => {
            let copy = (o1, o2, k) => {
                o1[k] = JSON.parse(JSON.stringify(o2[k]));
            }
            Object.keys(o2).forEach((k) => {
                let v2 = o2[k];
                if (!Object.prototype.hasOwnProperty.call(o1, k)) {
                    copy(o1, o2, k);
                } else if (['number', 'boolean', 'string'].includes(typeof (v2)) || v2 === undefined || v2 === null) {
                    o1[k] = v2;
                } else if (Array.isArray(v2)) {
                    copy(o1, o2, k);
                } else {
                    merge(o1[k], v2);
                }
            });
        };

        merge(nv, editorVal);
        return nv;
    },

    async saveAnno() {
        let anno = this.getEditorValue();
        console.log({anno});
        let graph = {};
        let isUpdate = Boolean(anno._id);
        anno._id =anno._id ||  '_:anno';

        let target = anno.target || null;
        let asChoice;

        if (!isUpdate && !target && this.asChoiceElem.checked && !this.context.disableChoice) {
            let choiceAnno = {
                jsonClass: 'ChoiceAnnotation',
                membersClass: anno.jsonClass,
                choices: {},
                target: this.context.target,
                _id: '_:ca',
            };
            target = choiceAnno._id;
            graph[choiceAnno._id] = choiceAnno;
            asChoice = true;
        }

        anno.target = target || this.context.target;
        graph[anno._id] = anno;

        this.statusElem.innerText = `${isUpdate ? 'updating' : 'creating'} annotation.....`;
        let resp;
        try {
            resp = await postGraph({
                kContext: this.kContext,
                graph,
                should_return_resources: true,
                upsert: true,
            });
            this.statusElem.innerText = `annotation ${isUpdate ? 'updated' : 'created'} successfully`;
        } catch(e) {
            this.statusElem.innerText = `annotation ${isUpdate ? 'update' : 'creation'} error`;
            throw e;
        }

        let detail = {
            anno: resp.graph[anno._id],
        };
        if (asChoice) {
            detail.choice = resp.graph[target];
        }
        let event = new CustomEvent('annoupsert', {
            detail,
        });
        this.rootElem.dispatchEvent(event);

        this.ensureVisibility(false);
        this.editor.destroy();
    }

}
