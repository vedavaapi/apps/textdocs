from textract_segmenters_common.celery_helper import make_celery

from .. import app
celery = make_celery(app, 'vvtextdocs.api.celery:celery', ['vvtextdocs.api.worker'])
