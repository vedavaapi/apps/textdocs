import json
import os

import flask
import flask_restplus
from lxml import etree
from requests import HTTPError

from vedavaapi.client import VedavaapiSession, ObjModelException, objstore

from ..markup import vvimporter
#  from ..markup.exporters import html as html_exporter
from ..helpers import scaaned_pages_html_exporter
from ..helpers.exporters import html as text_doc_html_exporter

from .celery import celery as celery_app
from .worker import scanned_book_textdoc_export_task, markup_import_task
from textract_segmenters_common import celery_helper

from . import api


def error_response(**kwargs):
    # function to construct all types of error response jsons
    error = {
        'code': 500  # default if not provided
    }
    if 'inherited_error_response' in kwargs:
        inherited_error_response = kwargs['inherited_error_response']
        error['code'] = inherited_error_response['error'].get('code', 500)
    error.update(kwargs)
    response = flask.make_response(
        flask.jsonify(error),  # json
        error['code']  # code
    )
    return response


def import_from_markup(vc, markup, parent=None, root_text_doc_id=None, attach_html=False):
    if not root_text_doc_id:
        root_text_doc = {
            "jsonClass": "TextDocument",
            "jsonClassLabel": "document",
        }
        if parent:
            root_text_doc['source'] = parent
        try:
            resp = vc.post('objstore/v1/resources', data={"resource_jsons": json.dumps([root_text_doc])})
            resp.raise_for_status()
        except HTTPError as e:
            return error_response(
                message='cannot create root text document', code=403,
                error={"response": e.response.json(), "status_code": e.response.status_code})
        root_text_doc_id = resp.json()[0]['_id']

    else:
        delete_success = vvimporter.delete_existing_children(vc, root_text_doc_id)
        if not delete_success:
            return error_response(message='error in deleting existing TextDocument hierarchy', code=403)

    try:
        root_text_doc_id, blank_graph, graph_id_uid_map, seq_members_map = vvimporter.import_from_markup_paginated(vc, markup, root_text_doc_id)
    except ValueError as e:
        #  raise e
        return error_response(message='invalid markup', code=400, error=str(e))
    except HTTPError as e:
        return error_response(
            message='error in posting text documents graph',
            code=403, error={"response": e.response.json(), "status_code": e.response.status_code}
        )
    response =  {
        "root_text_doc_id": root_text_doc_id
    }
    if attach_html:
        response['html'] = text_doc_html_exporter.export_from_blank_graph(
            blank_graph, graph_id_uid_map, seq_members_map
        )

    return response



# noinspection PyMethodMayBeStatic
@api.route('/')
@api.hide
class RootDocs(flask_restplus.Resource):

    def get(self):
        return flask.redirect('docs')


@api.route('/scanned_pages_text')
class ScannedPagesText(flask_restplus.Resource):

    post_parser = api.parser()
    post_parser.add_argument('site_url', location='form', required=True)
    post_parser.add_argument('access_token', location='form', required=True)
    post_parser.add_argument('book_id', location='form', required=False, default=None)
    post_parser.add_argument('page_ranges', location='form', required=False, default=None)
    post_parser.add_argument('page_ids', location='form', required=False, default=None)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()

        page_ranges = json.loads(args['page_ranges'] or 'null')
        if page_ranges != None and type(page_ranges) != list:
            return {"message": "invalid page_ranges", "code": 400}, 400
        if page_ranges and False in [type(item) in (list, int) for item in page_ranges]:
            return {"message": "invalid page_ranges", "code": 400}, 400

        page_ids = json.loads(args['page_ids'] or 'null')
        if page_ids != None and type(page_ids) != list:
            return {"message": "invalid page_ids", "code": 400}, 400

        if not args['book_id'] and not page_ids:
            return {"message": "book_id or page_ids have to be given", "code": 400}, 400

        vs = VedavaapiSession(args['site_url'])
        vs.set_access_token(args['access_token'])

        page_ids = page_ids or scaaned_pages_html_exporter.get_page_ids(vs, args['book_id'], page_ranges)

        try:
            html_doc_content = scaaned_pages_html_exporter.export_to_html(vs, page_ids)
        except ObjModelException as e:
            return error_response(message=e.message, code=e.status_code, attachments=e.attachments)

        inner_xml = ''.join(etree.tounicode(ie, method='html') for ie in html_doc_content)
        # inner_xml = etree.tounicode(text_document_content)

        return flask.make_response(inner_xml)


@api.route('/from_scanned_book_tasks')
class FromScannedBookTasks(flask_restplus.Resource):
    post_parser = api.parser()
    post_parser.add_argument('site_url', location='form', required=True)
    post_parser.add_argument('access_token', location='form', required=True)
    post_parser.add_argument('book_id', location='form', required=False, default=None)
    post_parser.add_argument('pages_range', location='form', required=False, default=None)
    post_parser.add_argument('page_ids', location='form', required=False, default=None)
    post_parser.add_argument('wrap_in_anno_spans', location='form', required=False, default='true', choices=['true', 'false'])
    post_parser.add_argument('oold_id', location='form', required=False, default=None)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()

        pages_range = json.loads(args['pages_range'] or 'null')
        if pages_range != None and type(pages_range) != list:
            return {"message": "invalid pages_range", "code": 400}, 400
        if pages_range and False in [type(item) in (int,) for item in pages_range]:
            return {"message": "invalid pages_range", "code": 400}, 400

        page_ids = json.loads(args['page_ids'] or 'null')
        if page_ids != None and type(page_ids) != list:
            return {"message": "invalid page_ids", "code": 400}, 400

        wrap_in_anno_spans = json.loads(args['wrap_in_anno_spans'])
        # print({"wias": wrap_in_anno_spans})

        if not args['book_id']:
            return {"message": "book_id or page_ids have to be given", "code": 400}, 400

        vs = VedavaapiSession(args['site_url'])
        vs.set_access_token(args['access_token'])

        #  print(pages_range, page_ids)
        task = scanned_book_textdoc_export_task.delay(
            vs, scanned_book_id=args['book_id'],
            pages_range=pages_range, pages_ids=page_ids, oold_id=args['oold_id'], wrap_in_spans=wrap_in_anno_spans
        )
        task_status_url = api.url_for(CeleryTask, task_id=task.id, _external=True)
        return {"status_url": task_status_url, "task_id": task.id}, 202, {
            'Location': task_status_url, 'Access-Control-Expose-Headers': 'Location'
        }


@api.route('/from_markup')
class FromMarkup(flask_restplus.Resource):

    post_parser = api.parser()
    post_parser.add_argument('site_url', location='form', required=True)
    post_parser.add_argument('access_token', location='form', required=True)
    post_parser.add_argument('parent_id', location='form', required=False, default=None)
    post_parser.add_argument('root_text_doc_id', location='form', required=False, default=None)
    post_parser.add_argument('markup', location='form', required=True)
    post_parser.add_argument(
        'attach_html', location='form', required=False, type=flask_restplus.inputs.boolean, default=False)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()

        vs = VedavaapiSession(args['site_url'])
        vs.set_access_token(args['access_token'])

        return import_from_markup(
            vs, args['markup'], parent=args.get('parent_id'), root_text_doc_id=args['root_text_doc_id'], attach_html=args['attach_html']
        )


@api.route('/from_markup_task')
class FromMarkupTask(flask_restplus.Resource):

    post_parser = api.parser()
    post_parser.add_argument('site_url', location='form', required=True)
    post_parser.add_argument('access_token', location='form', required=True)
    post_parser.add_argument('parent_id', location='form', required=False, default=None)
    post_parser.add_argument('root_text_doc_id', location='form', required=False, default=None)
    post_parser.add_argument('markup', location='form', required=True)
    post_parser.add_argument(
        'attach_html', location='form', required=False, type=flask_restplus.inputs.boolean, default=False)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()

        vs = VedavaapiSession(args['site_url'])
        vs.set_access_token(args['access_token'])

        task = markup_import_task.delay(
            vs, args['markup'], parent=args.get('parent_id'), root_text_doc_id=args['root_text_doc_id'], attach_html=args['attach_html']
        )
        task_status_url = api.url_for(CeleryTask, task_id=task.id, _external=True)
        return {"status_url": task_status_url, "task_id": task.id}, 202, {
            'Location': task_status_url, 'Access-Control-Expose-Headers': 'Location'
        }


@api.route('/<text_doc_id>/content')
class Content(flask_restplus.Resource):

    get_parser = api.parser()
    get_parser.add_argument('site_url', location='args', required=True)
    get_parser.add_argument('access_token', location='args', required=False, default=None)
    get_parser.add_argument('format', location='args', required=False, choices=('markup', 'json', 'html'), default='markup')

    @api.expect(get_parser, validate=True)
    def get(self, text_doc_id):
        args = self.get_parser.parse_args()
        vs = VedavaapiSession(args['site_url'])
        vs.set_access_token(args['access_token'])

        try:
            text_doc = objstore.get_resource(vs, text_doc_id, projection={"components": 0})
        except HTTPError as e:
            return error_response(
                code=e.response.status_code, message=str(e), error=e.response.json()
            )

        if args['format'] == 'markup':
            markup_data_repr = text_doc.get('markup')
            if (not markup_data_repr
                    or not isinstance(markup_data_repr, dict)
                    or not isinstance(markup_data_repr.get('data'), str)
                    or not markup_data_repr.get('data').startswith('_OOLD:')):
                markup = ''
            else:
                markup_oold_id = markup_data_repr['data'][6:]
                try:
                    markup_resp = vs.get('objstore/v1/files/{}'.format(markup_oold_id))
                    markup_resp.raise_for_status()
                except HTTPError as e:
                    return error_response(
                        message='error in getting markup', code=e.response.status_code, error=e.response.json()
                    )
                if markup_resp.encoding is None:
                    return error_response(message='invalid markup file encoding', code=400)
                markup = markup_resp.text
            return {"text_doc": text_doc, "markup": markup}

        elif args['format'] == 'html':
            html_content = text_doc_html_exporter.export(vs, text_doc_id)
            return {"text_doc": text_doc, "html": str(html_content)}

        else:
            return error_response(message='format not yet implemented', code=400)


# noinspection PyMethodMayBeStatic
@api.route('/files/<path:path>')
class StaticFile(flask_restplus.Resource):

    def get(self, path):
        (directory, fname) = os.path.split(__file__)
        files_dir = os.path.join(directory, 'static')
        return flask.send_from_directory(files_dir, path)


# noinspection PyMethodMayBeStatic
@api.route('/tasks/<task_id>')
class CeleryTask(flask_restplus.Resource):

    def get(self, task_id):
        return celery_helper.get_task_status(celery_app, task_id)

    def delete(self, task_id):
        celery_helper.terminate_task(celery_app, task_id)
        return {"task_id": task_id, "success": True}
