import flask_restplus
from flask import Blueprint


api_blueprint_v1 = Blueprint('textdocs_api' + '_v1', __name__)

api = flask_restplus.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='Vedavaapi TextDocuments Service',
    doc='/docs'
)


def push_environ_to_g():
    #  g.regions_detector = regions_detector
    pass


api_blueprint_v1.before_request(push_environ_to_g)


from . import rest
