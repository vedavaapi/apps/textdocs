import json

from celery.exceptions import Ignore
from requests import HTTPError, Response
from vedavaapi.client import VedavaapiSession, objstore

from .celery import celery as celery_app
from ..helpers import scanned_books_helper
from ..markup import vvimporter

from ..helpers.exporters import html as text_doc_html_exporter


@celery_app.task(bind=True)
def scanned_book_html_export_task(self, vc: VedavaapiSession, book_id=None, pages_range=None, page_ids=None, wrap_in_spans=True):
    self.update_state(state='PROGRESS', meta={"status": "initializing"})
    html_content = scanned_books_helper.export_as_html(
        vc, book_id=book_id, pages_range=pages_range, page_ids=page_ids, wrap_in_spans=wrap_in_spans,
        update_state=self.update_state
    )
    return {
        "html": html_content
    }


@celery_app.task(bind=True)
def scanned_book_textdoc_export_task(
        self, vc: VedavaapiSession,
        scanned_book_id=None, pages_range=None, pages_ids=None,
        oold_id=None, wrap_in_spans=True):

    ds_gid = oold_id or '_:ds1'
    ds_repr = {"jsonClass": "DataRepresentation", "data": f'_OOLD:{ds_gid}'}
    ds = {"jsonClass": "DataSet", "_id": ds_gid, "namespace": "_vedavaapi", "identifier": "ds.html", "source": scanned_book_id}
    book_update = {"jsonClass": "ScannedBook", "representations": {"jsonClass": "DataRepresentations", "default": "dataSet", "dataSet": [ds_repr]}}

    exported_html = scanned_books_helper.export_as_html(
        vc, book_id=scanned_book_id,
        pages_range=pages_range, page_ids=pages_ids,
        update_state=self.update_state, wrap_in_spans=wrap_in_spans
    )
    files = [("ds.html", exported_html)]
    try:
        response = objstore.post_graph(
            vc, { scanned_book_id: book_update }, ool_data_graph={ ds_gid: ds },
            files=files, should_return_resources=False
        )
        ds_id = response['ool_data_graph'][ds_gid]['_id']  # TODO
    except HTTPError as e:
        self.update_state(state='FAILURE', meta={
            "exc_type": "ObjModelException",
            "exc_message": ["error in posting text documents graph; code: {}; error: {}".format(
                e.response.status_code, e.response.json()
            )]
        })
        raise Ignore()

    return {"ds_id": ds_id}


@celery_app.task(bind=True)
def markup_import_task(self, vc, markup, parent=None, root_text_doc_id=None, attach_html=False):
    if not root_text_doc_id:
        root_text_doc = {
            "jsonClass": "TextDocument",
            "jsonClassLabel": "document",
        }
        if parent:
            root_text_doc['source'] = parent
        try:
            resp = vc.post('objstore/v1/resources', data={"resource_jsons": json.dumps([root_text_doc])})
            resp.raise_for_status()
        except HTTPError as e:
            self.update_state(state='FAILURE', meta={
                "exc_type": "HTTPError",
                "exc_message": ["cannot create root text document; code: {}; error: {}".format(
                    e.response.status_code, e.response.json()
                )]
            })
            raise Ignore()
        root_text_doc_id = resp.json()[0]['_id']

    else:
        delete_success = vvimporter.delete_existing_children(vc, root_text_doc_id)
        if not delete_success:
            self.update_state(state='FAILURE', meta={
                "exc_type": "HTTPError",
                "exc_message": ["error in deleting existing TextDocument hierarchy;"]
            })
            raise Ignore()

    try:
        root_text_doc_id, blank_graph, graph_id_uid_map, seq_members_map = vvimporter.import_from_markup_paginated(vc, markup, root_text_doc_id, update_state=self.update_state)
    except ValueError as e:
        self.update_state(state='FAILURE', meta={
            "exc_type": "MarkupError",
            "exc_message": ["invalid markup: " + str(e)]
        })
        raise Ignore()
    except HTTPError as e:
        self.update_state(state='FAILURE', meta={
            "exc_type": "HTTPError",
            "exc_message": ["error in posting text documents graph; code: {}; error: {}".format(e.response.status_code, e.response.json())]
        })
        raise Ignore()
    response =  {
        "root_text_doc_id": root_text_doc_id
    }
    if attach_html:
        response['html'] = text_doc_html_exporter.export_from_blank_graph(
            blank_graph, graph_id_uid_map, seq_members_map
        )
    return {
        "state": "SUCCESS", "status": "text document {} updated".format(root_text_doc_id), "result": response
    }
