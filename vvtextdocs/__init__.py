
import flask
from flask_cors import CORS

app = flask.Flask(__name__, instance_relative_config=True)
CORS(app)

try:
    app.config.from_json(filename="config.json")
except FileNotFoundError as e:
    pass

from .api import api_blueprint_v1
app.register_blueprint(api_blueprint_v1, url_prefix='')
