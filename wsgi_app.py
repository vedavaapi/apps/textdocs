import os
import sys
from werkzeug.debug import DebuggedApplication

(file_path, fname) = os.path.split(__file__)
app_path = file_path
if app_path:
  os.chdir(app_path)
sys.path.insert (0, app_path)


namespace_dir = os.path.normpath(os.path.join(app_path, '..'))
all_packages = ["textdocs", "vv-client-python"]
for package in all_packages:
  sys.path.insert(1, os.path.join(namespace_dir, package))


from vvtextdocs import app

application = DebuggedApplication(app)



if __name__ == '__main__':
  port = sys.argv[1] if len(sys.argv) > 1 else 5000
  application.app.run(host='0.0.0.0', port=port)
